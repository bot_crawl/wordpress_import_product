<?php
	class CPL_Admin {
		public function __construct(){
			
			add_action( 'admin_menu', array(&$this,'register_page') );
			add_action( 'init', array(&$this,'register_assets') );
			
		}

		public function register_assets(){
			if( is_admin() && isset($_GET['page']) && $_GET['page'] == 'import-products'){
				wp_enqueue_style( 'cpl-boostrap-css-1', CLP_URI . 'assets/bootstrap.css' );
				wp_enqueue_script( 'cpl-js-ajax', CLP_URI . 'assets/ajax_import.js', array( 'jquery' ) , "0.2");
			}
			wp_enqueue_style( 'cpl-css-1', CLP_URI . 'assets/cpl.css' );
			wp_enqueue_script( 'cpl-js-form', CLP_URI . 'assets/jquery.form.js', array( 'jquery' ) );
			
			// if( is_admin() && @$_GET['page'] == 'import-products' && @$_GET['extra_ajax'] == 1){
			// 	wp_enqueue_script( 'cpl-js-ajax', CLP_URI . 'assets/ajax_import.js', array( 'jquery' ) );
			// }

			
			
		}
		
		function register_page(){
			add_menu_page( 'Import Products', 'Import Products', 'manage_options', 'import-products', array(&$this,'cpl_admin_page'), CLP_URI.'assets/star.png', 6 ); 
		}

		function cpl_admin_page(){
			$options = get_option( 'clp_options');
			// var_dump($options);
			// die('test');
			// #Note 7
			// if( true ){
			if( $options['task'] == 'waiting' ){
				// Form for cron tab
			
				$alerts = $this->check_write_able();
				$errors = array();
				if( isset( $_POST['upload_method'] )  && $_POST['upload_method'] == 'normal'){
					$CPL_Data = new CLP_Data();
					$CPL_Data->_params  = $_POST;
					$CPL_Data->_files 	= $_FILES;
					$result = $CPL_Data->upload_file(false);
					if( $result['status'] == 0 ){
						$errors[] = $result['msg'];
					}else{
						$options = $result;
						unset( $options['msg'] );
						unset( $options['status'] );
						$options['task'] 		= 'read_file';
						$options['task_force'] 	= 'read_file';
						$options['force'] 		= 1;
						$options['parrent_cat'] = $_POST['parrent_cat'];
						// update_option( 'clp_options' ,$options);
						?>
							<script type="text/javascript">
								var url = "<?php echo home_url().'/wp-admin/admin.php?page=import-products&force=1'; ?>";
								window.location.href = url;
							</script>
						<?php

					}
				}

				?>
				<div style="clear:both;margin-top:10px;"></div>
				<?php
					if( count($alerts) > 0 || count($errors) ){
						?>
							<div class="container">       
								<div class="row">
									<div class="col-lg-12">
										<div class="panel panel-default">
											<div class="panel-heading">
												<h3 class="panel-title">Please fix folowing errors</h3>
											</div>
											<div class="panel-body">
												<ul>
													<?php
														if( count($alerts) > 0 ){
															foreach ($alerts as $alert) {
																echo '<li style="color:red;">'.$alert.'</li>';
															}	
														}
														if( count($errors) > 0 ){
															foreach ($errors as $error) {
																echo '<li style="color:red;">'.$error.'</li>';
															}	
														}
														
													?>
												</ul>
												
											</div>
										</div>
									</div>
								</div>
							</div>
						<?php
					}
				?>
				<div class="container">       
					<div class="row">
						<div class="col-lg-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title"><i class="fa fa-clock-o fa-fw"></i> Import Products</h3>
								</div>
								<div class="panel-body">
								  
									<?php 
										if( count($alerts) == 0 ){
											include_once CLP_ABSPATH.'views/dashboard-form.php';										
										}
									;?>
								   
								  
								</div>
							</div>
						</div>
						<div id="right">                        
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title"><i class="fa fa-clock-o fa-fw"></i> Processing</h3>
							</div>
							<div class="panel-body txt_process">
							   
							</div>
						</div>
						</div>
					</div>

				</div>
				<?php
			
			}else{
				$o_file 	= $options['file'];
				$o_sheet 	= (int)$options['sheet'];
				$o_key 		= (int)$options['key'];
				$o_percent 		= (int)$options['percent'];
				
				// $filename = CLP_ABSPATH.'pro_data/'.$file.'_'.$sheet.'_pro_data.txt';
				
				// if( file_exists($filename) ){				
					// include $filename;
				// }
				$path = CLP_ABSPATH.'pro_data';
				$new_data = array();
				if (is_dir($path) === true){
					$files = array_diff(scandir($path), array('.', '..'));
					if( count( $files ) > 0 ){
						$new_files = array();
						foreach ($files as $file) {	
							if( !empty($o_file) && strpos( $file, $o_file  ) === 0 ){
								$file_arr = explode( '_' , $file );
								$new_files[ $file_arr[1] ] = $file;
							}
						}
						
						if( count( $new_files ) > 0 ){
							ksort($new_files);
							$data = array();
							foreach( $new_files as $new_sheet => $new_file ){
								$filename = CLP_ABSPATH.'pro_data/'.$new_file;
								if( file_exists($filename) ){				
									include_once $filename;
									$data[] = $products_data;
								}
							}
							
							foreach( $data as $d_sheet => $arr_data ){
								foreach( $arr_data as $d_key => $arr_data_val ){
									$new_data[ $d_sheet ][ $d_key ] = ( isset( $arr_data_val['title'] ) ) ? $arr_data_val['title'] : $arr_data_val['handle'];
								}
							}
							
							
						}
					}
				}
				
				?>
				<div class="container">       
					<div class="row">
						<div class="col-lg-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title"><i class="fa fa-clock-o fa-fw"></i> Import Products By Ajax</h3>
								</div>
								<div class="panel-body">
								<?php 
									/*
									echo '<pre>';
									print_r($options);
									echo '</pre>';
									*/
								   ?>
								<?php
									if( count( $new_data ) > 0 ){
										foreach( $new_data as $n_sheet => $n_sheet_data ){
											if( $n_sheet <= $o_sheet ){
												?>
												<div class="progress" data-sheet="<?php echo $n_sheet; ?>" >
												  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="100"
												  aria-valuemin="0" aria-valuemax="100" style="width:100%">
													100% Complete
												  </div>
												</div>
												<?php
											}else{
												?>
												<div class="progress" data-sheet="<?php echo $n_sheet; ?>" >
												  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="0"
												  aria-valuemin="0" aria-valuemax="100" style="width:0%">
													0% Complete 
												  </div>
												</div>
												<?php
											}
										}
									}
								?>
								  
								</div>
							</div>
						</div>
						<!-- Start Right  -->
						<div id="right">    
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title"><i class="fa fa-clock-o fa-fw"></i> Processing</h3>
							</div>
							<div class="panel-body txt_process">
							   <ul class="list-group">
									<?php
										if( count( $new_data ) > 0 ){
											foreach( $new_data as $n_sheet => $n_sheet_data ){
												foreach( $n_sheet_data as $n_key => $pro_title ){
													if( $n_sheet <= $o_sheet ){
														$class = '';
														if( $n_key == $o_key ){
															$class = 'active';
														}
														?>
														<li class="list-group-item <?php echo $class;?>" data-sheet="<?php echo $n_sheet; ?>" data-key="<?php echo $n_key; ?>" ><?php echo $pro_title; ?><span class="badge">In Processing</span></li>
														<?php
													}else{
														?>
														<li class="list-group-item" data-sheet="<?php echo $n_sheet; ?>" data-key="<?php echo $n_key; ?>" ><?php echo $pro_title; ?><span class="badge">Waiting</span></li>
														<?php
													}
													
												}
											}
										}
									?>
								</ul>	
							</div>
						</div>
						</div>
						<!-- End Right -->
					</div>
				</div>	
				<script type="text/javascript">
				// Benthunder Ajax Import
				
				
				
				jQuery( document ).ready( function(){
					var task_import = new TaskImport("<?php echo get_site_url(); ?>");
						var pro_dones = jQuery('li.list-group-item.active').prevAll();
						jQuery.each( pro_dones , function( key,val ){
							jQuery(val).find('.badge').text('OK');
						});
						var o_sheet = '<?php echo $o_sheet; ?>';
						var o_percent = '<?php echo $o_percent; ?>';
						var active_progress = jQuery('.progress[data-sheet="'+ o_sheet +'"]').find('.progress-bar');
						active_progress.attr('aria-valuenow',o_percent);
						active_progress.attr('style','width: '+o_percent+'%');
						active_progress.addClass('progress-bar-info');
						active_progress.text(o_percent+'% complete');
						
					});
				</script>
				<?php
			}
		}

		function check_write_able(){
			$msg = array();
			if( !is_writeable( CLP_ABSPATH.'pro_data' ) ){
				$msg[] =  'Folder <strong>'.CLP_ABSPATH.'pro_data </strong> can not write able !' ;
			}
			if( !is_writeable( CLP_ABSPATH.'tmp_data' ) ){
				$msg[] =  'Folder <strong>'.CLP_ABSPATH.'tmp_data </strong> can not write able !' ;
			}
			if( !is_writeable( CLP_ABSPATH.'tmp_images' ) ){
				$msg[] =  'Folder <strong>'.CLP_ABSPATH.'tmp_images </strong> can not write able !' ;
			}

			if ( !is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
			  	$msg[] =  'Plugin <strong>Woocommerce</strong> is not actived or not installed. Please download it at <a target="_blank" href="https://wordpress.org/plugins/woocommerce/">Here</a> !' ;
			}

			return $msg;
		}
	}