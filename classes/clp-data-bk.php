<?php
	
	class CLP_Data{
		public $_params;
		public $_files;
		public $_return;
		
		function start(){
		
			
			$options = get_option( 'clp_options' );
			$this->_params['parrent_cat'] = $options['parrent_cat'];
			
			$task = $this->_params['task'];

			
			/* -----START FORCE HANDEL----- */
			if( isset( $this->_params['force'] ) && $this->_params['force'] == 1){
				$task = $this->_params['task_force'];	
				$pass = true;
				$msg = '';
				if( !$this->_params['file'] ){
					$msg = 'File is empty !';
					$pass = false;
				}else{
					if( !file_exists( CLP_ABSPATH.'tmp_data/'.$this->_params['file'] ) ){
						$msg = 'File is not exists !';
						$pass = false;
					}
				}

				if( !isset( $this->_params['sheet']) ){
					$msg = 'Sheet is empty !';
					$pass = false;
				}else{
					if( !is_numeric( $this->_params['sheet'] ) ){
						$msg = 'Sheet must be number !';
						$pass = false;
					}
				}

				if( !isset( $this->_params['key']) ){
					$msg = 'Key is empty !';
					$pass = false;
				}else{
					if( !is_numeric( $this->_params['sheet'] ) ){
						$msg = 'Key must be number !';
						$pass = false;
					}
				}

				if( $pass == false ){
					$this->_params = array();
					$this->_params['status'] 			= 0;
					$this->_params['sucess'] 			= 1;
					$this->_params['msg'] 				= '<span style="color:red;">'.$msg.'</span>';
					echo json_encode( $this->_params );
					die();	
				}

				unset($this->_params['force']);
				$this->_files['file_import']['name'] = $this->_params['file'];
				$this->_params['task'] = $this->_params['task_force'];
			}

			/* -----END FORCE HANDEL----- */

			if( $task != 'upload_file' && $task != 'save_setting'){
				update_option( 'clp_options' ,$this->_params);
			}
			
			switch ($task) {
				case 'save_setting':
					$this->save_setting();
					break;
				case 'upload_file':
					$this->upload_file();
					break;
				case 'read_file':
					$this->read_file();
					break;
				case 'process_data':
					$this->process_data();
					break;
				case 'delete_tmp_files':
					$this->delete_tmp_files();
					break;
				default:
					# code...
					break;
			}

			

		}
		
		function save_setting(){
			$options = get_option( 'clp_options' );
			$options['upload_method'] = $this->_params['upload_method'];
			$options['download_method'] = $this->_params['download_method'];
		
			update_option( 'clp_options' ,$options);
			$this->_params = array();
			$this->_params['status'] 			= 1;
			$this->_params['msg'] 				= 'Saving options done !';
			echo json_encode( $this->_params );
			die();
		}

		function delete_tmp_files(){
			$this->_delete_folder( CLP_ABSPATH.'pro_data' );	
			$this->_delete_folder( CLP_ABSPATH.'tmp_data' );	
			$this->_delete_folder( CLP_ABSPATH.'tmp_images' );
			$this->_params['status'] 			= 0;
			$this->_params['done'] 				= 1;
			$this->_params['sucess'] 			= 1;
			$this->_params['msg'] 				= 'All done !';
			echo json_encode( $this->_params );
			die();			
		}
		private function _delete_folder($path){
			if (is_dir($path) === true){
				$files = array_diff(scandir($path), array('.', '..'));
				foreach ($files as $file)
				{
					@unlink(realpath($path) . '/' . $file);
				}
			}
		}


		function upload_file($ajax = true){

		
			if( $this->_files ){
				
				if( empty( $this->_files['file_import']['name'] ) ){
					$this->_params = array();
					$this->_params['status'] 			= 0;
					$this->_params['sucess'] 			= 1;
					$this->_params['msg'] 				= '<span style="color:red;">File is empty !</span>';
					if( $ajax == true ){
						echo json_encode( $this->_params );
						die();
					}else{
						return $this->_params;
					}
				}

				$ext = $this->_files['file_import']['name'];
				$ext = explode('.',$ext);
				$ext = end($ext);
				$allow_exts = array('csv');
				if( !in_array($ext,$allow_exts) ){
					$this->_params = array();
					$this->_params['status'] 			= 0;
					$this->_params['sucess'] 			= 0;
					$this->_params['msg'] 				= '<span style="color:red;">File is not support !</span>';
					if( $ajax == true ){
						echo json_encode( $this->_params );
						die();
					}else{
						return $this->_params;
					}
				}else{
					$new_file = time().'.'.$ext;
					$uploaded = copy( $this->_files['file_import']['tmp_name'], CLP_ABSPATH.'tmp_data/'.$new_file);
					if( !$uploaded ){
						$this->_params = array();
						$this->_params['status'] 			= 0;
						$this->_params['sucess'] 			= 0;
						$this->_params['msg'] 				= '<span style="color:red;">Can not upload file !</span>';
						if( $ajax == true ){
							echo json_encode( $this->_params );
							die();
						}else{
							return $this->_params;
						}
					}else{
						$this->_params['status'] 			= 1;
						$this->_params['msg'] 				= 'Uploaded file !';
						$this->_params['action'] 			= 'clp_admin_start';
						$this->_params['task'] 				= 'read_file';
						$this->_params['key'] 				= 0;
						$this->_params['file'] 				= $new_file;
						$this->_params['sheet'] 			= 0;
						if( $ajax == true ){
							echo json_encode( $this->_params );
							die();
						}else{
							return $this->_params;
						}
					}
				}
			}else{
				$this->_params = array();
				$this->_params['status'] 			= 0;
				$this->_params['sucess'] 			= 1;
				$this->_params['msg'] 				= '<span style="color:red;">No file is uploaded !</span>';
				if( $ajax == true ){
					echo json_encode( $this->_params );
					die();
				}else{
					return $this->_params;
				}
			}
		}
		function read_file(){
			$tmp_direct = CLP_ABSPATH .'tmp_data/';
			$file 		= $tmp_direct.$this->_params['file'];
			$sheetIndex = $this->_params['sheet'];
			
			
			
			if( file_exists( $file ) ){
				$ext_arr 	= explode('.',$file);
				$ext 		= end($ext_arr);
				if( $ext == 'csv' ){
					$this->handle_file_csv( $file,$sheetIndex );
				}else{
					$this->handle_file( $file,$sheetIndex );
				}
				
			}else{
				$this->show_errros();
			}

			//$this->_params['task'] = 'handle_file';
		}
		

		

		function handle_file( $file,$sheetIndex=0){
			error_reporting(E_ALL);
			ini_set('display_errors', TRUE);
			ini_set('display_startup_errors', TRUE);
			define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
			$sheet 	  = $this->_params['sheet'];
			$start    = $sheet * 100;
			$limit 	  = $start + 100;

			$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
    		$cacheSettings = array( 'memoryCacheSize ' => '256MB');
    		PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
			
			$inputFileType = 'Excel2007';
			$inputFileName = $file;

			$chunkSize = 100;
    		$chunkFilter = new chunkReadFilter();

			$inputFileType 	= PHPExcel_IOFactory::identify($inputFileName);
			$objReader 		= PHPExcel_IOFactory::createReader($inputFileType);

			$chunkFilter->setRows($start,$chunkSize);
    		$objReader->setReadFilter($chunkFilter);
     		$objReader->setReadDataOnly(true);

			$objPHPExcel 	= $objReader->load($inputFileName);
			$sheet 			= $objPHPExcel->getSheet(0); 
			
			$highestColumn 	= $sheet->getHighestColumn();

			$sheetData = array();
			for ($row = $start; $row < $limit; $row++){ 
			    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
			                                    NULL,
			                                    TRUE,
			                                    FALSE);
			   	$sheetData[] = $rowData[0];
			}

			
			if( $start == 0 ){
				unset($sheetData[0]);
				unset($sheetData[1]);
			}
			$curent_sheetData[0] = '';
			if( count( $sheetData ) > 0 ){
				$curent_sheetData = current($sheetData);
			}


			if( count( $sheetData ) > 0 && $curent_sheetData[0] != "" ){
				$new_data = array();
				foreach ($sheetData as $key => $data) {

					if( $data[0] != "" ){
						$new_data[$data[0]]['handle'] 	 	= $data[0];
						
						if( $data[1] != "" ){
							$new_data[$data[0]]['title'] 	 	= $data[1];
						}
						if( $data[4] != "" ){
							$new_data[$data[0]]['product_cat'] 	 	= $data[4];
						}
						$new_data[$data[0]]['image_src'] 	= ( $data[22]  ) ? $data[22] : $data[40];
						
						if( $data[5] != "" ){
							$new_data[$data[0]]['tags'] 	 	= $data[5];
						}
						if( trim($data[11]) != "" ){
							$new_data[$data[0]]['sku'] 	 		= $data[11];
						}
						if( trim($data[28]) != "" ){
							$new_data[$data[0]]['g_categories'] = $data[28];
						}
						if( trim($data[2]) != "" ){
							$new_data[$data[0]]['description']  = $data[2];
						}
						if( trim($data[3]) != "" ){
							$new_data[$data[0]]['manufacture']  = $data[3];
						}

						$v_image = ( isset($data[40]) && $data[40] != "") ? $data[40] : $data[22];
						if( empty($v_image) ){
							$v_image = $data[22];
						}
						if( empty($v_image) ){
							$v_image = $new_data[$data[0]]['image_src'];
						}
						$v_price = $data[17];
						$v_grams = $data[12];
						$v_stock = $data[14];
						
						$v_image = 
						
						$new_data[$data[0]]['v_color'][] 	 	= $data[8];
						$new_data[$data[0]]['v_size'][] 	 	= $data[10];
						$new_data[$data[0]]['v_image'][] 	 	= $v_image;
						$new_data[$data[0]]['v_price'][] 	 	= $v_price;
						$new_data[$data[0]]['v_grams'][] 	 	= $v_grams;	
						$new_data[$data[0]]['v_stock'][] 	 	= $v_stock;	
					}
				}
				/* Write array data into txt file */
				$time 		= date('Y_m_d',time());
				$sheetData 	= array_values( $new_data );

				
				$str 		= '<?php';
				$str 		.= ' $products_data = ' . var_export($sheetData, true) . ';';
				$filename 	= $this->_params['file'].'_'.($this->_params['sheet']).'_pro_data.txt';
				file_put_contents( CLP_ABSPATH .'pro_data/'.$filename ,$str);

				$this->_params['task'] 				= 'read_file';
				$this->_params['sheet'] 			= $this->_params['sheet'] + 1;
				$this->_params['msg']				= 'Complete readfile start '.$start.' limit '.$limit;
				$this->_params['status'] 			= 1;
				$this->_params['key'] 				= 0;
				$this->_params['file'] 				= $this->_params['file'];
			}else{
				$this->_params['task'] 				= 'process_data';
				$this->_params['sheet'] 			= 0;
				$this->_params['msg']				= 'Starting process data !';
				$this->_params['status'] 			= 1;
				$this->_params['key'] 				= 0;
			}
			
			echo json_encode( $this->_params );
			die();
		}

		function process_data(){
			
			$file 	= $this->_params['file'];
			$sheet 	= $this->_params['sheet'];
			$key 	= $this->_params['key'];

			$filename = CLP_ABSPATH.'pro_data/'.$file.'_'.$sheet.'_pro_data.txt';
			$this->_params['msg']				= 'Something wrong !';
			$this->_params['percent']				= 100;
			if( file_exists($filename) ){				
				include $filename;

				if( isset( $products_data[ $key ] )  &&  $products_data[ $key ] != NULL){
					if( !empty( $products_data[ $key ]['handle']) && $products_data[ $key ]['image_src'] !== NULL && $products_data[ $key ]['image_src'] != "" ){
						xdebug_start_trace();
						$pro_id = $this->_import_product( $products_data[ $key ] );
						xdebug_stop_trace();
						if( $pro_id ){
							$this->_params['msg']				= 'Imported product '.$products_data[ $key ]['title'].'. Inserted ID: '.$pro_id;
							$this->_params['key']				= $key + 1;
							$this->_params['percent']			= ( $key + 1 ) / count($products_data) * 100;
							$this->_params['percent']			= floor($this->_params['percent']);
							$this->_params['status'] 			= 1;
							$this->_params['sheet']				= $sheet;
							
						}else{
							$this->_params['key']				= $key + 1;
							$this->_params['percent']			= ( $key + 1 ) / count($products_data) * 100;
							$this->_params['percent']			= floor($this->_params['percent']);
							$this->_params['status'] 			= 1;
							$this->_params['sheet']				= $sheet;
							$this->_params['msg']				= 'Something wrong !. Continue on new product.';
							
						}
					}else{
						$this->_params['key']				= $key + 1;
						$this->_params['percent']			= ( $key + 1 ) / count($products_data) * 100;
						$this->_params['percent']			= floor($this->_params['percent']);
						$this->_params['status'] 			= 1;
						$this->_params['sheet']				= $sheet;
						$this->_params['msg']				= 'Something wrong !. Continue on new product.';
					}
				}else{
					$this->_params['msg']				= 'Imported products completed. Continue on new file';
					$this->_params['sheet']				= $sheet + 1;
					$this->_params['status'] 			= 1;
					$this->_params['percent']			= 100;
					$this->_params['key']				= 0;
				}
			}else{
				$this->_params['status'] 			= 1;
				$this->_params['task'] 				= 'delete_tmp_files';
				$this->_params['sucess'] 			= 1;
				$this->_params['key']				= 0;
				$this->_params['percent']				= 100;
				$this->_params['sheet']				= 0;
				$this->_params['file']				= '';
				$this->_params['msg']				= $file.' have no sheet <strong>'.$sheet.'</strong> or <strong>import-product/pro_data/'.$file.'_'.$sheet.'_pro_data.txt</strong> was not created on first step !';
				
			}
			echo json_encode( $this->_params );
			die();
		}
		public function detectDelimiter($csvFile){
		    $delimiters = array(
		        ';' => 0,
		        ',' => 0,
		        "\t" => 0,
		        "|" => 0
		    );

		    $handle = fopen($csvFile, "r");
		    $firstLine = fgets($handle);
		    fclose($handle); 
		    foreach ($delimiters as $delimiter => &$count) {
		        $count = count(str_getcsv($firstLine, $delimiter));
		    }

		    return array_search(max($delimiters), $delimiters);
		}
		function handle_file_csv( $file, $sheetIndex = 0 ){
			$sheet 	  = $this->_params['sheet'];
			$start    = $sheet * 1000;
			$limit 	  = $start + 1000;



			$d = $this->detectDelimiter($file);

			$sheetData = array();
			if (($handle = fopen($file, "r")) !== FALSE) {
			  $k = $start;
			  $i = 0;	


			  while (($data = fgetcsv($handle, 1000, $d)) !== FALSE) {
			  	if ($i++ <= $start) {
			        continue;
			    }
			    $sheetData[] = $data;
				if( $k == $limit ){
			  		break;	
			  	}
				$k++;
			  } 
			  fclose($handle);
			}

			if( count( $sheetData ) > 0 ){
				$new_data = array();
				foreach ($sheetData as $key => $data) {

					if( $data[0] != "" ){
						$new_data[$data[0]]['handle'] 	 	= $data[0];
						
						if( $data[1] != "" ){
							$new_data[$data[0]]['title'] 	 	= $data[1];
						}
						if( $data[4] != "" ){
							$new_data[$data[0]]['product_cat'] 	 	= $data[4];
						}
						$new_data[$data[0]]['image_src'] 	= ( $data[22]  ) ? $data[22] : $data[40];
						
						if( $data[5] != "" ){
							$new_data[$data[0]]['tags'] 	 	= $data[5];
						}
						if( trim($data[11]) != "" ){
							$new_data[$data[0]]['sku'] 	 		= $data[11];
						}
						if( trim($data[28]) != "" ){
							$new_data[$data[0]]['g_categories'] = $data[28];
						}
						if( trim($data[2]) != "" ){
							$new_data[$data[0]]['description']  = $data[2];
						}
						if( trim($data[3]) != "" ){
							$new_data[$data[0]]['manufacture']  = $data[3];
						}

						$v_image = ( isset($data[40]) && $data[40] != "") ? $data[40] : $data[22];
						if( empty($v_image) ){
							$v_image = $data[22];
						}
						if( empty($v_image) ){
							$v_image = $new_data[$data[0]]['image_src'];
						}
						$v_price = $data[17];
						$v_grams = $data[12];
						$v_stock = $data[14];
						
				
						
						$new_data[$data[0]]['v_color'][] 	 	= $data[8];
						$new_data[$data[0]]['v_size'][] 	 	= $data[10];
						$new_data[$data[0]]['v_image'][] 	 	= $v_image;
						$new_data[$data[0]]['v_price'][] 	 	= $v_price;
						$new_data[$data[0]]['v_grams'][] 	 	= $v_grams;	
						$new_data[$data[0]]['v_stock'][] 	 	= $v_stock;	
					}
				}
				
				/* Write array data into txt file */
				$time 		= date('Y_m_d',time());
				$sheetData 	= array_values( $new_data );

				
				$str 		= '<?php';
				$str 		.= ' $products_data = ' . var_export($sheetData, true) . ';';
				$filename 	= $this->_params['file'].'_'.($this->_params['sheet']).'_pro_data.txt';
				file_put_contents( CLP_ABSPATH .'pro_data/'.$filename ,$str);

				$this->_params['task'] 				= 'read_file';
				$this->_params['sheet'] 			= $this->_params['sheet'] + 1;
				$this->_params['msg']				= 'Complete readfile start '.$start.' limit '.$limit;
				$this->_params['status'] 			= 1;
				$this->_params['key'] 				= 0;
				$this->_params['file'] 				= $this->_params['file'];
			}else{
				$this->_params['task'] 				= 'process_data';
				$this->_params['sheet'] 			= 0;
				$this->_params['msg']				= 'Starting process data !';
				$this->_params['status'] 			= 1;
				$this->_params['key'] 				= 0;
			}
			
			echo json_encode( $this->_params );
			die();
			
		}

		function _import_product( $data ){
			global $wpdb;
			$CLP_Helper = new CLP_Helper();
			$options = get_option( 'clp_options' );
			$download_method = $options['download_method'];

			if( $download_method == "" || $download_method == 'download' ){
				$download_method = 'download';
			}else{
				$download_method = 'cdn';
			}
			
			$download_method = 'cdn';
			
			
			$parrent_category_id 		= $this->_params['parrent_cat'];
			$categories 				= $data['g_categories'];
			$product_name				= $data['handle'];
			
			$product_name				= $CLP_Helper->createSlug($product_name);
			$product_name				= sanitize_title($product_name);
			$title						= $data['title'];
			
			$post_content				= ( $data['description'] ) ? $data['description'] : '';
			$product_manufacture		= $data['manufacture'];
			$product_featured_img	 	= $data['image_src'];
			$product_featured_galeries = array();
			
			
			$check_exist_pro_id = $this->_check_exist_pro( $product_name );

		
			
			
			$post_id = 0;
			if ( !empty($check_exist_pro_id) ) {
				$pro_id = $check_exist_pro_id;
			}else{
				$pro_id = $this->_insert_post( $title,$product_name );	
			}

			

			//if( $post_id && empty( $check_exist_post_id )){
			if( $pro_id ){
				wp_set_object_terms($pro_id, 'variable', 'product_type');
				if( taxonomy_exists('product_cat') == true ){					
					if( $data['product_cat'] != "" ){
						$product_cat = array();
						$term_id = $this->_get_cat_id(	
							htmlentities( trim( $data['product_cat'] ) ),
							$parrent_category_id
						);
						$product_cat[$key] = $term_id;
						wp_set_post_terms($pro_id, $product_cat, 'product_cat');
					}
				}


				$tags = explode(',', $data['tags'] );	
				if( is_array( $tags ) && count( $tags ) > 0 ){
					foreach( $tags as $tag ){
						if( $tag != "" ){
							$tag_id = $this->_get_attribute_id(	
								htmlentities( trim( $tag ) ),
								'product_tag',
								0
							);
							if( $tag_id ){
								$product_tag[] = $tag_id;
							}
							
						}
					}
					wp_set_object_terms( $pro_id, $product_tag, 'product_tag' );
				}
			
				$attributes = get_post_meta( $pro_id, '_product_attributes', true );
				/* Get manufatory for product */
				if( taxonomy_exists('pa_manufacturers') == true ){
					if( $data['manufacture'] != ""){
						$factory_id = $this->_get_attribute_id(	
							htmlentities( $data['manufacture'] ),
							'pa_manufacturers',
							0
						);

						$product_factory = array(
							(int)$factory_id
						);
						wp_set_object_terms( $pro_id, $product_factory, 'pa_manufacturers' );
						
						$attributes[ 'pa_manufacturers' ] = array(
							'name' 			=> 'pa_manufacturers',
							'value' 		=> $product_factory,
							'position' 		=> 0,
							'is_visible' 	=> 1,
							'is_variation' 	=> 0,
							'is_taxonomy' 	=> 1
						);
						update_post_meta( $pro_id, '_product_attributes', $attributes );
					}
					
				}
				
				
				
				if( taxonomy_exists('pa_color') == true ){
					if( is_array($data['v_color']) && count($data['v_color']) > 0){
						$v_colors = array_unique($data['v_color']);
						$color_ids = array();
						foreach ($v_colors as $v_color) {
							$color_ids[] = $this->_get_attribute_id(	
								htmlentities( $v_color ),
								'pa_color',
								0
							);
						}	


						wp_set_object_terms( $pro_id, $color_ids, 'pa_color' );
						$attributes[ 'pa_color' ] = array(
							'name' 			=> 'pa_color',
							'value' 		=> '',
							'position' 		=> 1,
							'is_visible' 	=> 0,
							'is_variation' 	=> 1,
							'is_taxonomy' 	=> 1
						);
						update_post_meta( $pro_id, '_product_attributes', $attributes );					
					}
				}
				
				if( taxonomy_exists('pa_size') == true ){
					if( is_array($data['v_size']) && count($data['v_size']) > 0){
						$v_sizes = array_unique($data['v_size']);
						$size_ids = array();
						foreach ($v_sizes as $v_size) {
							$size_ids[] = $this->_get_attribute_id(	
								htmlentities( $v_size ),
								'pa_size',
								0
							);
						}
						wp_set_object_terms( $pro_id, $size_ids, 'pa_size' );
						$attributes['pa_size'] = array(
							'name' 			=> 'pa_size',
							'value' 		=> '',
							'position' 		=> 2,
							'is_visible' 	=> 0,
							'is_variation' 	=> 1,
							'is_taxonomy' 	=> 1
						);
						update_post_meta( $pro_id, '_product_attributes', $attributes );					
					}
				}


				/* SET VARIANT FOR VARIABLES */


				
				if( is_array($data['v_color']) && count($data['v_color']) > 0){
					$v_colors  = $data['v_color'];
					$v_stocks  = $data['v_stock'];
					$v_weights = $data['v_grams'];
					$v_sizes   = $data['v_size'];
					$v_prices  = $data['v_price'];
					
					$v_images  = $data['v_image'];
					foreach ($v_colors as $key => $v_color) {
						$v_size    = $v_sizes[$key];
						$v_price   = $v_prices[$key];
						$v_price   = str_replace(',','.', $v_price);
						
						if( !get_post_meta($pro_id,'_default_attributes',true) ){
							$default_attributes = array(
								'pa_color' => $CLP_Helper->createSlug($v_color),
								'pa_size' => $CLP_Helper->createSlug($v_size)
							);
							update_post_meta($pro_id,'_default_attributes',$default_attributes);
						}
						

						$post_name = 'product-' . $pro_id . '-color-' .  $CLP_Helper->createSlug($v_color).'-size-'.$CLP_Helper->createSlug($v_size);
						$my_post = array(
							'post_title' => 'Color: ' . $v_color . ' - Size: '.$v_size.'  for #' . $pro_id,
							'post_name' => $post_name,
							'post_status' => 'publish',
							'post_parent' => $pro_id,
							'post_type' => 'product_variation',
							'guid' => home_url() . '/?product_variation=' . $post_name
						);
						
						$sql = "SELECT ID FROM $wpdb->posts WHERE post_name = '".sanitize_text_field($post_name)."' AND post_type = 'product_variation'";
						$attID = $wpdb->get_var($sql);

						if ($attID < 1) {
							$attID = wp_insert_post($my_post);
						}

						update_post_meta($attID,'custom_price',$v_price );	

						$v_image_id = 0;
						$v_featured_img = $v_images[$key];
						
						if( $v_featured_img){

							
							 $v_featured_img = str_replace('=w1500-h1400','=w800-h400', $v_featured_img);
							 $v_product_featured_link = $v_featured_img; 
							 $v_featured_img_name = explode('//',$v_featured_img);
							 $v_featured_img_name = end($v_featured_img_name);
							 $v_featured_img_name = $CLP_Helper->createSlug($v_featured_img_name);

							
							$sql = "SELECT post_id FROM $wpdb->postmeta WHERE  meta_key = 'v_featured_img_name' AND meta_value = '".sanitize_text_field($v_featured_img_name)."'";
							$v_featured_pro_id = $wpdb->get_var($sql);
							if ($v_featured_pro_id < 1) {
								if( $download_method == 'download' ){
									$v_featured_img = $this->_download_img_from_url($v_featured_img);
									if( file_exists( $v_featured_img ) ){
										$v_featured_img_id 		= $this->_insert_attachment_post( $attID,$v_featured_img,$v_featured_img_name );
										if( $v_featured_img_id ){
											$v_product_featured_link = wp_get_attachment_image_url($v_featured_img_id);
											update_post_meta($attID,'_thumbnail_id',$v_featured_img_id );	
											update_post_meta($attID,'v_featured_img_name',$v_featured_img_name );	
											update_post_meta($attID,'custom_img_url',$v_product_featured_link );	
										}
									}
								}else{
									$v_featured_img_id 		= $this->_insert_cdn_attachment_post( $attID,$v_featured_img,$v_featured_img_name );
									if( $v_featured_img_id ){
										update_post_meta($attID,'_thumbnail_id',$v_featured_img_id );	
										update_post_meta($v_featured_img_id,'is_cdn',1 );	
										update_post_meta($attID,'v_featured_img_name',$v_featured_img_name );
										update_post_meta($attID,'custom_img_url',$v_product_featured_link );	
									}
								}
							}else{
								$sql = "SELECT meta_value FROM $wpdb->postmeta WHERE post_id = $v_featured_pro_id AND meta_key = '_thumbnail_id'";
								$v_featured_img_id = $wpdb->get_var($sql);
								update_post_meta($attID,'_thumbnail_id',$v_featured_img_id );	

								$v_product_featured_link = wp_get_attachment_image_url($v_featured_img_id);
								update_post_meta($attID,'custom_img_url',$v_product_featured_link );
							}
						 }
						 

						update_post_meta($attID, 'attribute_pa_color', $CLP_Helper->createSlug($v_color) );
						update_post_meta($attID, 'attribute_pa_size', $CLP_Helper->createSlug($v_size) );
						update_post_meta($attID, '_price', $v_price );
						update_post_meta($attID, '_regular_price', $v_price );
						
						update_post_meta($attID, '_sku', $post_name );
						update_post_meta($attID, '_virtual', 'no');
						update_post_meta($attID, '_downloadable', 'no');
						update_post_meta($attID, '_manage_stock', 'no');
						update_post_meta($attID, '_stock', $v_stocks[$key] );
						update_post_meta($attID, '_stock_status', 'instock');
						update_post_meta($attID, '_weight', $v_weights[$key] );
					}

					
					if( isset($v_prices[0]) && $v_prices[0] != "" ){
						$v_prices[0] = str_replace(',','.',$v_prices[0]);
						update_post_meta( $pro_id,'custom_price',$v_prices[0] );
					}
					if( isset($v_colors[0]) && $v_colors[0] != "" ){
						update_post_meta($pro_id, 'attribute_pa_color', $CLP_Helper->createSlug($v_colors[0]) );
					}

					if( isset($v_sizes[0]) && $v_sizes[0] != "" ){
						update_post_meta($pro_id, 'attribute_pa_size', $CLP_Helper->createSlug($v_sizes[0]) );
					}
					
				 	
				 	
				}
				/* Updata product meta */

				
				

				$product_sku 	= ( $data['sku'] != "" ) ? $data['sku'] : $CLP_Helper->createSlug($product_name);
				
				 update_post_meta( $pro_id, '_visibility', 'visible' );
				 update_post_meta( $pro_id, '_price', get_post_meta( $pro_id,'custom_price',true) );
				 update_post_meta( $pro_id, '_stock_status', 'instock');
				 update_post_meta( $pro_id, '_manage_stock', 'no');
				 update_post_meta( $pro_id, 'total_sales', '0');
				 update_post_meta( $pro_id, '_downloadable', 'no');
				 update_post_meta( $pro_id, '_virtual', 'no');
				 update_post_meta( $pro_id, '_purchase_note', "" );
				 update_post_meta( $pro_id, '_featured', "no" );
				 update_post_meta( $pro_id, '_length', "" );
				 update_post_meta( $pro_id, '_width', "" );
				 update_post_meta( $pro_id, '_height', "" );
				 update_post_meta( $pro_id, '_sku', $product_sku);
				 update_post_meta( $pro_id, '_sale_price_dates_from', "" );
				 update_post_meta( $pro_id, '_sale_price_dates_to', "" );
				 update_post_meta( $pro_id, '_sold_individually', "" );
				 update_post_meta( $pro_id, '_backorders', "no" );
				
				 /* Updata product description */	
				 
				if( $product_featured_img ){
					$product_featured_img 	= get_redirect_target($product_featured_img);
					 $product_featured_img 	= str_replace('=w1500-h1400','=w800-h400', $product_featured_img);
					 //$product_featured_img = explode('?',$product_featured_img);
					 $product_featured_link = $product_featured_img;

					 $product_featured_img_name = $product_featured_img;
					 $product_featured_img_name = explode('//',$product_featured_img_name);
					 $product_featured_img_name = end($product_featured_img_name);
					 $product_featured_img_name = $CLP_Helper->createSlug($product_featured_img_name);
					
					
					$sql = "SELECT post_id FROM $wpdb->postmeta WHERE  meta_key = 'featured_img_name' AND meta_value = '".sanitize_text_field($product_featured_img_name)."'";

					$featured_pro_id = $wpdb->get_var($sql);

					if ($featured_pro_id < 1) {
						if( $download_method == 'download' ){
							$product_featured_img = $this->_download_img_from_url($product_featured_img);
							if( file_exists( $product_featured_img ) ){
								$attach_id 		= $this->_insert_attachment_post( $pro_id,$product_featured_img,$product_sku );
								if( $attach_id ){
									$product_featured_link = wp_get_attachment_image_url($attach_id);
									update_post_meta($pro_id,'_thumbnail_id',$attach_id );	
									update_post_meta($pro_id,'featured_img_name',$product_featured_img_name );	
									update_post_meta($pro_id,'custom_img_url',$product_featured_link );	
								}
							}
						}else{
							$attach_id 		= $this->_insert_cdn_attachment_post( $pro_id,$product_featured_img,$product_sku );

							if( $attach_id ){
								update_post_meta($pro_id,'_thumbnail_id',$attach_id );	
								update_post_meta($pro_id,'featured_img_name',$product_featured_img_name );
								update_post_meta($pro_id,'custom_img_url',$product_featured_link );		
								update_post_meta($attach_id,'is_cdn',1 );	
							}
						}
						
					}else{
						$sql = "SELECT meta_value FROM $wpdb->postmeta WHERE post_id = $featured_pro_id AND meta_key = '_thumbnail_id'";
						$featured_img_id = $wpdb->get_var($sql);
						update_post_meta($pro_id,'_thumbnail_id',$featured_img_id );
						$product_featured_link = wp_get_attachment_image_url($featured_img_id);	
						update_post_meta($pro_id,'custom_img_url',$product_featured_link );
					}					
				}
				 
				

				if( $post_content != '' ){
					$pee = $post_content;
					$pee = preg_replace('|<br\s*/?>\s*<br\s*/?>|', "", $pee);
 
				    $allblocks = '(?:table|thead|tfoot|caption|col|colgroup|tbody|tr|td|th|div|dl|dd|dt|ul|ol|li|pre|form|map|area|blockquote|address|math|style|p|h[1-6]|hr|fieldset|legend|section|article|aside|hgroup|header|footer|nav|figure|figcaption|details|menu|summary)';
				 
				    // Add a double line break above block-level opening tags.
				    $pee = preg_replace('!(<' . $allblocks . '[\s/>])!', "$1", $pee);
				 
				    // Add a double line break below block-level closing tags.
				    $pee = preg_replace('!(</' . $allblocks . '>)!', "$1", $pee);
				    // Standardize newline characters to "\n".
   					$pee = str_replace(array("\r\n", "\r"), "", $pee);
					$update_post = array(
					  'ID'           	=> $pro_id,
					  'post_content'   	=> $pee
					);
					wp_update_post( $update_post );
				}

						


				/* PUSH DATA TO GOOGLE SHEET */
				require_once CLP_ABSPATH.'/libraries/process.php';
				$push_data['id'] 			= $pro_id;
				$push_data['title'] 		= $title;
				$push_data['description'] 	= $post_content;
				$push_data['link'] 			= get_the_permalink($pro_id);
				$push_data['producttype'] 	= $data['product_cat'];
				$push_data['agegroup'] 		= 'adult';
				if( $categories == '' ){
					$categories = 'Apparel & Accessories > Clothing > Shirts & Tops';
				}
				/*
				$push_data['googleproductcategory'] 	= 'Apparel & Accessories > Clothing > Shirts & Tops';
				*/
				$push_data['googleproductcategory'] 	= $categories;
				$push_data['condition'] 	= 'New';
				$push_data['price'] 		= get_post_meta( $pro_id,'custom_price',true);
				$push_data['imagelink'] 	= get_post_meta( $pro_id,'custom_img_url',true);
				$push_data['availability'] 	= 'In Stock';
				$push_data['gtin'] 			= '';
				$push_data['mpn'] 			= '';
				$push_data['brand'] 		= $product_manufacture;
				
				$push_data['gender'] 			= 'unisex';
				$push_data['shippingweight'] 	= '188';
				$push_data['color'] 			= get_post_meta($pro_id, 'attribute_pa_color',true);
				$push_data['size'] 				= get_post_meta($pro_id, 'attribute_pa_size',true);
				push_to_sheet($push_data);
				
				return $pro_id;
			}
		}
		
		function _download_img_from_url($img,$i=''){
			$ext = explode('.',$img);
			$ext = end($ext);

			$image_name = explode('//',$img);
			$image_name = end($image_name);

			$type_image = 'image/jpeg';
			if( $ext == 'png' ){
				$type_image = 'image/png';
			}

			$url = $img;
			$CLP_Helper = new CLP_Helper();
			$image_name = str_replace($ext,'', $image_name);
			$image_name = str_replace('--','-', $image_name);
			$image_name = $CLP_Helper->createSlug($image_name);
			$image_name = $image_name.'.'.$ext;

			$img = CLP_ABSPATH.'tmp_images/'.$image_name;
			file_put_contents($img, file_get_contents($url));
			return $img;
		}
		
		function _insert_attachment_galery_post($post_id,$images,$product_sku){
			$i = 1;
			$attach_ids = array();
			foreach ($images as $image) {
				if( file_exists($image) ){
					$attach_ids[] = $this->_insert_attachment_post($post_id,$image,$product_sku,$i );
					$i++;
				}
			}

			if( count($attach_ids) > 0){
				$attach_ids = implode(',',$attach_ids);
				update_post_meta($post_id,'_product_image_gallery',$attach_ids);

				$attach_ids = explode(',',$attach_ids);
			}
			return $attach_ids;
		}

		private function _insert_cdn_attachment_post($post_id,$product_featured_img,$product_sku = '',$i = 0 ){
			$ext = explode('.',$product_featured_img);
			$ext = end($ext);

			$type_image = '	image/jpeg';
			if( $ext == 'png' ){
				$type_image = 'image/png';
			}
			$tmp_file_info = array(
				'name' 		=> $product_sku.'.'.$ext,
				'type'		=> $type_image,
				'tmp_name' 	=> $product_featured_img,
			);
			$name = $tmp_file_info['name'];
			$attachment = array(
	          'post_mime_type' => $type_image,
	          'post_title' => preg_replace('/\.[^.]+$/', '', $name ),
	          'post_name' => $product_sku,
	          'post_content' => '',
	          'guid' => $product_featured_img,
	          'post_status' => 'inherit'
	        );

			

	        $attach_id = wp_insert_attachment($attachment, $product_featured_img, $post_id);

	        return $attach_id;

		}

		function tool_upload_dir($uploads ){
			$uploads['baseurl'] 	= '';
		    return $uploads;
		}
		private function _insert_attachment_post($post_id,$product_featured_img,$product_sku = '',$i = 0 ){

			$ext = explode('.',$product_featured_img);
			$ext = end($ext);

			$type_image = '	image/jpeg';
			if( $ext == 'png' ){
				$type_image = 'image/png';
			}

			$tmp_file_info = array(
				'name' 		=> $product_sku.'.'.$ext,
				'type'		=> $type_image,
				'tmp_name' 	=> $product_featured_img,
			);
			
			$upload_path = wp_upload_dir();
			$name = $tmp_file_info['name'];
			if ( @copy( $tmp_file_info['tmp_name'], $upload_path['path'] . '/' . $name ) ) {
				@unlink( $product_featured_img );
			}
	        $wp_filetype = wp_check_filetype( $upload_path['path'] . '/' . $name , NULL);
	        $pathurl = $upload_path['url'];
	        $attachment = array(
	          'post_mime_type' => $wp_filetype['type'],
	          'post_title' => preg_replace('/\.[^.]+$/', '', $name ),
	          'post_name' => $product_sku,
	          'post_content' => '',
	          'post_status' => 'inherit'
	        );
	       
	      
	        $attach_id   = wp_insert_attachment($attachment, $upload_path['path'] . '/' . $name, $post_id);	       
	        $attach_data = wp_generate_attachment_metadata($attach_id, $upload_path['path'] . '/' . $name);
	        wp_update_attachment_metadata($attach_id, $attach_data);
	        return $attach_id;
		}

		private function _check_exist_pro( $post_title ){
			global $wpdb;
			$sql = "SELECT ID FROM $wpdb->posts WHERE post_name =  '".$post_title."' AND post_type = 'product'";	
			$check_exist_pro_id   = $wpdb->get_var( $sql );
			return $check_exist_pro_id;
		}

		private function _insert_post($pro_title , $pro_name ){
			$data['post_title'] 	= sanitize_text_field( $pro_title );
			$post_name = ( $pro_name != "" ) ? $pro_name : $pro_title;
			$data['post_name'] 		= $post_name;
		    $data['post_status'] 	= 'publish';
		    $data['post_author'] 	= 1;
		    $data['post_type'] 		= 'product';
		    $post_id = wp_insert_post($data);
		    return $post_id;
		}
		private function _get_cat_id( $category_name,$parent_term_id = 0 ){
			$taxonomy = 'product_cat';
			$term = get_term_by('name', $category_name, $taxonomy );
			
			if( !$term ){
				$args = array('parent' => $parent_term_id);
				$term_name = $category_name;
				$term_id = wp_insert_term( 
					$term_name, 
					$taxonomy ,
					$args
					);
				return $term_id['term_id'];
			}else{
				return $term->term_id;
			}
			
		}
		private function _get_attribute_id( $attribute_name,$taxonomy = 'pa_manufacturers',$parent_term_id = 0 ){
			if( $attribute_name != "" ){
				$term = get_term_by('name', $attribute_name, $taxonomy );

				if( !$term ){
					$args = array('parent' => $parent_term_id);
					$term_name = $attribute_name;
					$term_id = wp_insert_term( 
						$term_name, 
						$taxonomy ,
						$args
						);
					return $term_id['term_id'];
				}else{
					return $term->term_id;
				}
			}
		}
		private function _get_manufactory_id( $factory_name,$parent_term_id = 0 ){
			$taxonomy = 'pa_manufacturers';
			$term = get_term_by('name', $factory_name, $taxonomy );

			
			if( !$term ){
				$args = array('parent' => $parent_term_id);
				$term_name = $factory_name;
				$term_id = wp_insert_term( 
					$term_name, 
					$taxonomy ,
					$args
					);
				return $term_id['term_id'];
			}else{
				return $term->term_id;
			}
		}
}

class chunkReadFilter implements PHPExcel_Reader_IReadFilter
{
	private $_startRow = 0;
	private $_endRow = 0;

	public function setRows($startRow, $chunkSize) {
	    $this->_startRow    = $startRow;
	    $this->_endRow      = $startRow + $chunkSize;
	}

	public function readCell($column, $row, $worksheetName = '') {
	    if (($row == 1) || ($row >= $this->_startRow &&$row < $this->_endRow)) {
	        return true;
	    }
	    return false;
	}
}