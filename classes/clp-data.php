<?php
	class CLP_Data{
		public $_params;
		public $_files;
		public $_return;
		public $_tmp_index;
		

		// Bat dau chay
		// Task nao thi xu ly cai do

		function start(){
			// Lưu khoảng thừa vào trong seesion
			if(!isset($_SESSION['tmp_index'])){
				$_SESSION['tmp_index'] = 0;
			}
			$this->_tmp_index = $_SESSION['tmp_index'];

			$options = get_option( 'clp_options' );
			$this->_params['parrent_cat'] = "0";


			
			$task = $this->_params['task'];

			/* -----START FORCE HANDEL----- */
			if( isset( $this->_params['force'] ) && $this->_params['force'] == 1){
				$task = $this->_params['task_force'];	
				$pass = true;
				$msg = '';
				if( !$this->_params['file'] ){
					$msg = 'File is empty !';
					$pass = false;
				}else{
					if( !file_exists( CLP_ABSPATH.'tmp_data/'.$this->_params['file'] ) ){
						$msg = 'File is not exists !';
						$pass = false;
					}
				}

				if( !isset( $this->_params['sheet']) ){
					$msg = 'Sheet is empty !';
					$pass = false;
				}else{
					if( !is_numeric( $this->_params['sheet'] ) ){
						$msg = 'Sheet must be number !';
						$pass = false;
					}
				}

				if( !isset( $this->_params['key']) ){
					$msg = 'Key is empty !';
					$pass = false;
				}else{
					if( !is_numeric( $this->_params['sheet'] ) ){
						$msg = 'Key must be number !';
						$pass = false;
					}
				}

				if( $pass == false ){
					$this->_params = array();
					$this->_params['status'] 			= 0;
					$this->_params['sucess'] 			= 1;
					$this->_params['msg'] 				= '<span style="color:red;">'.$msg.'</span>';
					echo json_encode( $this->_params );
					exit();	
				}

				unset($this->_params['force']);
				$this->_files['file_import']['name'] = $this->_params['file'];
				$this->_params['task'] = $this->_params['task_force'];
			}

			/* -----END FORCE HANDEL----- */

			if( $task != 'upload_file' && $task != 'save_setting'){
				update_option( 'clp_options' ,$this->_params);
			}
			
			switch ($task) {
				case 'waiting':
					echo 'Waiting task';
					exit();
					break;
				case 'save_setting':
					$this->save_setting();
					break;
				case 'upload_file':
					$this->upload_file();
					break;
				case 'read_file':
					$this->read_file();
					break;
				case 'process_data':
					$this->process_data();
					break;
				case 'delete_tmp_files':
					$this->delete_tmp_files();
					break;
				default:
					# code...
					break;
			}

			

		}
	
		
		// Luu setting
		function save_setting(){
			$options = get_option( 'clp_options' );
			$options['upload_method'] = $this->_params['upload_method'];
			$options['download_method'] = $this->_params['download_method'];
		
			update_option( 'clp_options' ,$options);
			$this->_params = array();
			$this->_params['status'] 			= 1;
			$this->_params['msg'] 				= 'Saving options done !';
			echo json_encode( $this->_params );
			exit();
		}
		
		
		// Xoa file CSV da upload
		function delete_tmp_files(){
			delete_option('colum_names');
			$this->_delete_folder( CLP_ABSPATH.'pro_data' );	
			$this->_delete_folder( CLP_ABSPATH.'tmp_data' );	
			$this->_delete_folder( CLP_ABSPATH.'tmp_images' );
			$this->_params['status'] 			= 0;
			$this->_params['done'] 				= 1;
			$this->_params['sucess'] 			= 1;
			$this->_params['msg'] 				= 'All done !';
			
			$this->sendmail_aws( $this->_params['msg']  );
			
			$options['task'] = 'waiting';
			update_option( 'clp_options' ,$options);
			
			echo json_encode( $this->_params );
			exit();			
		}
		
		// Gui mail API
		private function sendmail_aws($meg)
		{
			$url = 'http://edm.kiennhay.vn/api/campaigns/private.php';
			
			$site_title = get_bloginfo( 'name' );
			$html_content = "<h1>Alert !!</h1>";
			$html_content .= "<p>$meg</p>";
			   $data = array(
			  'api_key'           =>'J8TN6qY7ndeGAkWeeVqv',
			  'from_name'         =>'Thông Báo Import',
			  'to_email'          =>'alldevelopshop@gmail.com',
			  'to_name'           =>'Admin Shop',
			  'reply_to'          =>'alldevelopshop@gmail.com',
			  'subject'           =>'[Import Cronjob Process - '.$site_title.' ]',
			  'plain_text'        =>$meg,
			  'html_text'         =>$html_content,
			);
			$options = array(
				'http' => array(
					'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
					'method'  => 'POST',
					'content' => http_build_query($data),
				),
			);
			$context  = stream_context_create($options);
			$result = file_get_contents($url, false, $context);
			
			return true;
		}
		// Xoa Thu Muc
		private function _delete_folder($path){
			if (is_dir($path) === true){
				$files = array_diff(scandir($path), array('.', '..'));
				foreach ($files as $file)
				{
					@unlink(realpath($path) . '/' . $file);
				}
			}
		}


		// Xu ly upload file
		function upload_file($ajax = true){
			delete_option('colum_names');
			if( $this->_files ){
				
				if( empty( $this->_files['file_import']['name'] ) ){
					$this->_params = array();
					$this->_params['status'] 			= 0;
					$this->_params['sucess'] 			= 1;
					$this->_params['msg'] 				= '<span style="color:red;">File is empty !</span>';
					if( $ajax == true ){
						echo json_encode( $this->_params );
						exit();
					}else{
						return $this->_params;
					}
				}

				$ext = $this->_files['file_import']['name'];
				$ext = explode('.',$ext);
				$ext = end($ext);
				$allow_exts = array('csv');
				if( !in_array($ext,$allow_exts) ){
					$this->_params = array();
					$this->_params['status'] 			= 0;
					$this->_params['sucess'] 			= 0;
					$this->_params['msg'] 				= '<span style="color:red;">File is not support !</span>';
					if( $ajax == true ){
						echo json_encode( $this->_params );
						exit();
					}else{
						return $this->_params;
					}
				}else{
					$new_file = time().'.'.$ext;
					$uploaded = copy( $this->_files['file_import']['tmp_name'], CLP_ABSPATH.'tmp_data/'.$new_file);
					if( !$uploaded ){
						$this->_params = array();
						$this->_params['status'] 			= 0;
						$this->_params['sucess'] 			= 0;
						$this->_params['msg'] 				= '<span style="color:red;">Can not upload file !</span>';
						if( $ajax == true ){
							echo json_encode( $this->_params );
							exit();
						}else{
							return $this->_params;
						}
					}else{
						$this->_params['status'] 			= 1;
						$this->_params['msg'] 				= 'Uploaded file !';
						$this->_params['action'] 			= 'clp_admin_start';
						$this->_params['task'] 				= 'read_file';
						$this->_params['key'] 				= 0;
						$this->_params['file'] 				= $new_file;
						$this->_params['sheet'] 			= 0;
						if( $ajax == true ){
							echo json_encode( $this->_params );
							exit();
						}else{
							return $this->_params;
						}
					}
				}
			}else{
				$this->_params = array();
				$this->_params['status'] 			= 0;
				$this->_params['sucess'] 			= 1;
				$this->_params['msg'] 				= '<span style="color:red;">No file is uploaded !</span>';
				if( $ajax == true ){
					echo json_encode( $this->_params );
					exit();
				}else{
					return $this->_params;
				}
			}
		}

		// Doc file va [tach file] --> handle_file_csv()
		function read_file(){
			$tmp_direct = CLP_ABSPATH .'tmp_data/';
			$file 		= $tmp_direct.$this->_params['file'];
			$sheetIndex = $this->_params['sheet'];
			
			
			
			if( file_exists( $file ) ){
				$ext_arr 	= explode('.',$file);
				$ext 		= end($ext_arr);
				if( $ext == 'csv' ){
					$this->handle_file_csv( $file,$sheetIndex );
				}else{
					$this->show_errros();
				}
				
			}else{
				$this->show_errros();
			}

			//$this->_params['task'] = 'handle_file';
		}
		
		// #UnknowFunction
		// ??? Cai nay la cai giong' gi`
		function process_data(){
				
			$file 	= $this->_params['file'];
			$sheet 	= $this->_params['sheet'];
			$key 	= $this->_params['key'];



			
			$filename = CLP_ABSPATH.'pro_data/'.$file.'_'.$sheet.'_pro_data.txt';


			$this->_params['msg']				= 'Something wrong !';
			$this->_params['percent']				= 100;
			if( file_exists($filename) ){				
				include $filename;
				if( isset( $products_data[ $key ] )  &&  $products_data[ $key ] != NULL){
					
					if( !empty( $products_data[ $key ]['handle']) && !empty( $products_data[ $key ]['title']) && $products_data[ $key ]['image_src'] !== NULL && $products_data[ $key ]['image_src'] != "TRUE" ){
						$pro_id = $this->_import_product( $products_data[ $key ] );
						if( $pro_id ){
							$this->_params['msg']				= 'Imported product '.$products_data[ $key ]['title'].'. Inserted ID: '.$pro_id;
							$this->_params['key']				= $key + 1;
							$this->_params['percent']			= ( $key + 1 ) / count($products_data) * 100;
							$this->_params['percent']			= floor($this->_params['percent']);
							$this->_params['status'] 			= 1;
							$this->_params['sheet']				= $sheet;
							
						}else{
							$this->_params['key']				= $key + 1;
							$this->_params['percent']			= ( $key + 1 ) / count($products_data) * 100;
							$this->_params['percent']			= floor($this->_params['percent']);
							$this->_params['status'] 			= 1;
							$this->_params['sheet']				= $sheet;
							$this->_params['msg']				= 'Something wrong !. Continue on new product.';
							
						}
					}else{
						
						/* Write to log */
						$log_file = CLP_ABSPATH.'log.txt';
						$log_title = ( isset( $products_data[ $key ]['title'] ) ) ? $products_data[ $key ]['title'] : $products_data[ $key ]['handle'];
						$log_title = ($log_title ) ? $log_title : 'Unknown name';
						$log_content = '[ '.date( 'd-m-Y H:i:s',time() ).' ] '.$log_title. ' can not import !';
						$log_content = $log_content."\r\n";
						$myfile = fopen($log_file,"a");
						fwrite($log_file, $log_content);
						// some code to be executed....
						fclose($log_file);
					
						$this->_params['key']				= $key + 1;
						$this->_params['percent']			= ( $key + 1 ) / count($products_data) * 100;
						$this->_params['percent']			= floor($this->_params['percent']);
						$this->_params['status'] 			= 1;
						$this->_params['sheet']				= $sheet;
						$this->_params['msg']				= 'Something wrong !. Continue on new product.';
					}
				}else{
					$this->_params['msg']				= 'Imported products completed. Continue on new file';
					$this->_params['sheet']				= $sheet + 1;
					$this->_params['status'] 			= 1;
					$this->_params['percent']			= 100;
					$this->_params['key']				= 0;
				}
			}else{
				$this->_params['status'] 			= 1;
				$this->_params['task'] 				= 'delete_tmp_files';
				$this->_params['sucess'] 			= 1;
				$this->_params['key']				= 0;
				$this->_params['percent']				= 100;
				$this->_params['sheet']				= 0;
				$this->_params['file']				= '';
				$this->_params['msg']				= $file.' have no sheet <strong>'.$sheet.'</strong> or <strong>import-product/pro_data/'.$file.'_'.$sheet.'_pro_data.txt</strong> was not created on first step !';
				
			}
			update_option('clp_options',$this->_params);
			
			echo json_encode( $this->_params );
			global $wpdb;
			$wpdb->close();
			exit();
		}

		public function detectDelimiter($csvFile){
		    $delimiters = array(
		        ';' => 0,
		        ',' => 0,
		        "\t" => 0,
		        "|" => 0
		    );

		    $handle = fopen($csvFile, "r");
		    $firstLine = fgets($handle);
		    fclose($handle); 
		    foreach ($delimiters as $delimiter => &$count) {
		        $count = count(str_getcsv($firstLine, $delimiter));
		    }

		    return array_search(max($delimiters), $delimiters);
		}


		// [tach file]
		function handle_file_csv( $file, $sheetIndex = 0 ){
			if(is_null($this->_tmp_index)){
				$this->_tmp_index = 0;
			}
			$sheet 	  = $this->_params['sheet'];
			$start    =  ($sheet < 1) ? ($sheet * 1000) : (($sheet *1000) - $this->_tmp_index);
			$limit 	  =  ($sheet * 1000) + 1000;

			$CLP_Helper = new CLP_Helper();



			$d = $this->detectDelimiter($file);

			$sheetData = array();
			if (($handle = fopen($file, "r")) !== FALSE) {
			  $k = $start;
			  $i = 0;	

			  $colum_names = get_option('colum_names');
			  if( ! $colum_names ){
					$header = fgetcsv($handle, 1000, $d);
					foreach ($header as $key_h => $header_val) {
						$header_val 	= str_replace('body-(html)', 'body-html', $header_val);
						$colum_names[] 	= $CLP_Helper->createSlug($header_val);
					}
					if( count($colum_names) > 0 ){
						update_option('colum_names',$colum_names);
					}
			  }
			  
			  while (($data = fgetcsv($handle, 2000, $d)) !== FALSE) {
				if ($i++ <= $start) {
			        continue;
				}
				// if ($k++ > ($start +900) && trim($data['1']) !== "" ){
				// 	$this->_tmp_index = ($k - $start);
				// 	break;
				// }
				
				if( $k == $limit || $k > ($start + 900) && trim($data[1]) !== "" ){
					// if($sheet > 4 ){
						// 	var_dump($k , $start , $limit);
						// 	die("Note Die"'break');
						// }
						$_SESSION['tmp_index'] = ($limit - ($k +1) ) ;
						$this->_params['var_dum'] = [
							"k"=>$k,
							"start"=>$start,
							"limit"=>$limit,
							"tmp_index"=>$this->_tmp_index,
							"sesesion" => ($_SESSION['tmp_index']),
							"data" => $data,
							"limit - k = "=> ($limit - ($k+1))
						];
						// $this->_tmp_index = $_SESSION['tmp_index']
						$limit = $k ;
						break;	
					}
					$sheetData[] = $data;
					$k++;
				}
			  fclose($handle);
			}

			$new_sheetData = array();
			if( count( $sheetData ) > 0 ){
				
				foreach ($sheetData as $key => $data_arr) {
					$new_data = array();
					foreach ($data_arr as $key_data => $value_data) {
						$colum_names[$key_data] 	= str_replace('body-(html)', 'body-html',$colum_names[$key_data]);
						$new_data[$colum_names[$key_data]] = $value_data;
					}
					$new_sheetData[] = $new_data;
				}
			}
			$sheetData = $new_sheetData;

			
			// var_dump(count($sheetData));

			if( count( $sheetData ) > 0 ){
				$new_data = array();
				foreach ($sheetData as $key => $data) {

					if( $data['handle'] != "" ){
						$handle = $data['handle'];
						$new_data[$handle]['handle'] 	 	= $data['handle'];
						
						if( $data['title'] !== "" ){
							$new_data[$handle]['title'] 	 	= $data['title'];
						}
						if( $data['type'] != "" ){
							$new_data[$handle]['product_cat'] 	 	= $data['type'];
						}
						$new_data[$handle]['image_src'] 	= ( $data['variant-image'] != ""  ) ? $data['variant-image'] : $data['image-src'];
						
						if( $data['tags'] != "" ){
							$new_data[$handle]['tags'] 	 	= $data['tags'];
						}
						if( trim($data['variant-sku']) != "" ){
							$new_data[$handle]['sku'] 	 		= $data['variant-sku'];
						}
						if( trim($data['google-shopping-google-product-category']) != "" ){
							$new_data[$handle]['g_categories'] = $data['google-shopping-google-product-category'];
						}
						if( trim($data['body-html']) !== "" ){
							$new_data[$handle]['description']  = $data['body-html'];
						}
						if( trim($data['vendor']) != "" ){
							$new_data[$handle]['manufacture']  = $data['vendor'];
						}

						$v_image = ( isset($data['variant-image']) && $data['variant-image'] != "") ? $data['variant-image'] : $data['image-src'];
						if( empty($v_image) ){
							$v_image = $data['image-src'];
						}
						if( empty($v_image) ){
							$v_image = $new_data[$handle]['image_src'];
						}
						$v_price = $data['variant-price'];
						$v_grams = $data['variant-grams'];
						$v_stock = $data['variant-inventory-qty'];
						
				
						
						$new_data[$handle]['v_color'][] 	 	= $data['option1-value'];
						$new_data[$handle]['v_size'][] 	 		= $data['option2-value'];
						$new_data[$handle]['v_image'][] 	 	= $v_image;
						$new_data[$handle]['v_price'][] 	 	= $v_price;
						$new_data[$handle]['v_grams'][] 	 	= $v_grams;	
						$new_data[$handle]['v_stock'][] 	 	= $v_stock;	
					}


				}

				/* Write array data into txt file */
				$time 		= date('Y_m_d',time());
				$sheetData 	= array_values( $new_data );

				$str 		= '<?php';
				$str 		.= ' $products_data = ' . var_export($sheetData, true) . ';';
				$filename 	= $this->_params['file'].'_'.($this->_params['sheet']).'_pro_data.txt';
				file_put_contents( CLP_ABSPATH .'pro_data/'.$filename ,$str);

				$this->_params['task'] 				= 'read_file';
				$this->_params['sheet'] 			= $this->_params['sheet'] + 1;
				$this->_params['msg']				= 'Complete readfile start '.($start) .' limit '.($limit);
				$this->_params['status'] 			= 1;
				$this->_params['key'] 				= 0;
				$this->_params['file'] 				= $this->_params['file'];
			}else{
				$this->_params['task'] 				= 'process_data';
				$this->_params['sheet'] 			= 0;
				$this->_params['msg']				= 'Starting process data !';
				$this->_params['status'] 			= 1;
				$this->_params['key'] 				= 0;
			}
			var_dump('test');
			update_option( 'clp_options' , $this->_params);
			
			echo json_encode( $this->_params );
			exit();
			
		}


		// import Product
		function _import_product( $data ){
			global $wpdb;
			$CLP_Helper = new CLP_Helper();
			$options = get_option( 'clp_options' );
			$download_method = 'cdn';
			
			
			$parrent_category_id 		= $this->_params['parrent_cat'];
			$categories 				= $data['g_categories'];
			$product_handle				= $data['handle'];
			$product_name				= $data['handle'];
			
			$product_name				= $CLP_Helper->createSlug($product_name);
			$product_name				= sanitize_title($product_name);
			$title						= ( $data['title'] ) ? $data['title'] : $product_name;
			
			$post_content				= ( $data['description'] ) ? $data['description'] : '';
			$product_manufacture		= $data['manufacture'];
			$product_featured_img	 	= $data['image_src'];
			$product_featured_galeries = array();
			
			
			$check_exist_pro_id = $this->_check_exist_pro( $product_name );
			$push_to_sheet = true;
			
			
			$post_id = 0;
			if ( !empty($check_exist_pro_id) ) {
				$pro_id = $check_exist_pro_id;
				$push_to_sheet = false;
			}else{
				$pro_id = $this->_insert_post( $title,$product_name );	
			}

			

			//if( $post_id && empty( $check_exist_post_id )){
			if( $pro_id ){
				wp_set_object_terms($pro_id, 'variable', 'product_type');
				if( taxonomy_exists('product_cat') == true ){					
					if( $data['product_cat'] != "" ){
						$product_cat = array();
						$term_id = $this->_get_cat_id(	
							htmlentities( trim( $data['product_cat'] ) ),
							$parrent_category_id
						);
						$product_cat[] = $term_id;
						wp_set_post_terms($pro_id, $product_cat, 'product_cat');
					}
				}



				$tags = explode(',', $data['tags'] );	


			

				if( is_array( $tags ) && count( $tags ) > 0 ){
					$product_tag = array();
					foreach( $tags as $tag ){
						if( $tag != "" ){
							$tag_id = $this->_get_attribute_id(	
								htmlentities( trim( $tag ) ),
								'product_tag',
								0
							);

						
							if( $tag_id ){
								$product_tag[] = $tag_id;
							}
							
						}
					}
					if( count( $product_tag)  > 0){
						wp_set_object_terms( $pro_id, $product_tag, 'product_tag' );
					}
					
				}



			
				$attributes = get_post_meta( $pro_id, '_product_attributes', true );
				$data_attributes = [];
				/* Get manufatory for product */
				if( taxonomy_exists('pa_manufacturers') == true ){
					if( $data['manufacture'] != ""){
						$factory_id = $this->_get_attribute_id(	
							htmlentities( $data['manufacture'] ),
							'pa_manufacturers',
							0
						);

						if( $factory_id ){
							$product_factory = array(
								(int)$factory_id
							);
							wp_set_object_terms( $pro_id, $product_factory, 'pa_manufacturers' );
							
							$attributes[ 'pa_manufacturers' ] = array(
								'name' 			=> 'pa_manufacturers',
								'value' 		=> '',
								'position' 		=> 0,
								'is_visible' 	=> 1,
								'is_variation' 	=> 0,
								'is_taxonomy' 	=> 1
							);
							update_post_meta( $pro_id, '_product_attributes', $attributes );
						}

						
					}
					$data_attributes['attribute_values'][0] = $product_factory;
					$data_attributes['attribute_names'][0] = 'pa_manufacturers';
					$data_attributes['attribute_position'][0] = 0;
					
					
				}
				
				
				
				if( taxonomy_exists('pa_color') == true ){
					if( is_array($data['v_color']) && count($data['v_color']) > 0){
						$v_colors = array_unique($data['v_color']);
						$color_ids = array();
						foreach ($v_colors as $v_color) {
							$color_id = $this->_get_attribute_id(	
								htmlentities( $v_color ),
								'pa_color',
								0
							);
							if( $color_id ){
								$color_ids[] = $color_id;
							}
							
						}

						if( count($color_ids) > 0 ){
							wp_set_object_terms( $pro_id, $color_ids, 'pa_color' );
							$attributes[ 'pa_color' ] = array(
								'name' 			=> 'pa_color',
								'value' 		=> '',
								'position' 		=> 1,
								'is_visible' 	=> 0,
								'is_variation' 	=> 1,
								'is_taxonomy' 	=> 1
							);
							update_post_meta( $pro_id, '_product_attributes', $attributes );			
						}

						$data_attributes['attribute_values'][1] = $color_id;
						$data_attributes['attribute_names'][1] = 'pa_color';
						$data_attributes['attribute_position'][1] = 1;
						$data_attributes['attribute_visibility'][1] = 1;
						$data_attributes['attribute_variation'][1] = 1;
								
					}
				}


				if( taxonomy_exists('pa_size') == true ){
					if( is_array($data['v_size']) && count($data['v_size']) > 0){
						$v_sizes = array_unique($data['v_size']);
						$size_ids = array();
						foreach ($v_sizes as $v_size) {
							$size_id = $this->_get_attribute_id(	
								htmlentities( $v_size ),
								'pa_size',
								0
							);
							if( $color_id ){
								$size_ids[] = $size_id;
							}

						}
						if( count($size_ids) > 0){
							wp_set_object_terms( $pro_id, $size_ids, 'pa_size' );
							$attributes['pa_size'] = array(
								'name' 			=> 'pa_size',
								'value' 		=> '',
								'position' 		=> 2,
								'is_visible' 	=> 0,
								'is_variation' 	=> 1,
								'is_taxonomy' 	=> 1
							);
							update_post_meta( $pro_id, '_product_attributes', $attributes );	
						}
						
						$data_attributes['attribute_values'][2] = $size_ids;
						$data_attributes['attribute_names'][2] = 'pa_size';
						$data_attributes['attribute_position'][2] = 2;
						$data_attributes['attribute_visibility'][1] = 1;
						$data_attributes['attribute_variation'][1] = 1;
										
					}
				}

			

				/* SET VARIANT FOR VARIABLES */
				
				
				

				$sql_insert = "";
				if( is_array($data['v_color']) && count($data['v_color']) > 0){
					$v_colors  = $data['v_color'];
					$v_stocks  = $data['v_stock'];
					$v_weights = $data['v_grams'];
					$v_sizes   = $data['v_size'];
					$v_prices  = $data['v_price'];
					
					$v_images  = $data['v_image'];
					foreach ($v_colors as $key => $v_color) {
						$v_size    = isset( $v_sizes[$key] ) ? $v_sizes[$key] : 0;
						if( !$v_size ){
							continue;
						}
						$v_price   = $v_prices[$key];
						$v_price   = str_replace(',','.', $v_price);
						if( $key == 0 ){
							$default_attributes = array(
								'pa_color' => $CLP_Helper->createSlug($v_color),
								'pa_size' => $CLP_Helper->createSlug($v_size)
							);
							update_post_meta($pro_id,'_default_attributes',$default_attributes);
							$product_featured_img = $v_images[0];
						}
						$post_name = 'product-' . $pro_id . '-color-' .  $CLP_Helper->createSlug($v_color).'-size-'.$CLP_Helper->createSlug($v_size);
						$sql = "SELECT ID FROM $wpdb->posts WHERE post_name = '".sanitize_text_field($post_name)."' AND post_type = 'product_variation' AND post_parent = ".$pro_id;
						$attID = $wpdb->get_var($sql);
						if( wp_delete_post($attID) ){
							$attID = 0;
						}
						if ($attID < 1 ) {
							global $wpdb;
						    $attID = $wpdb->insert( 
								$wpdb->posts, 
								array( 
									'post_title' => 'Color: ' . $v_color . ' - Size: '.$v_size.'  for #' . $pro_id,
									'post_name' => $post_name,
									'post_status' => 'publish',
									'post_type' => 'product_variation',
									'guid' => home_url() . '/?product_variation=' . $post_name,
									'post_parent' => $pro_id,
								), 
								array( 
									'%s', 
									'%s', 
									'%s', 
									'%s', 
									'%s', 
									'%d' 
								) 
							);
							if( $attID ){
								$attID = $wpdb->insert_id;
						   }
						}
						$v_featured_img = $v_images[$key];
						if( $v_featured_img){
							// Add Image for variant
							if(preg_match("/drive.google.com/", $v_featured_img)){
								$v_featured_img = str_replace('=w1500-h1400','=w800-h400', $v_featured_img);
							}

							$v_product_featured_link = $v_featured_img; 
							update_post_meta($attID,'custom_img_url',$v_product_featured_link );	
							update_post_meta($attID,'_thumbnail_id',$attID );	
							update_post_meta($attID,'_wp_attached_file',$v_product_featured_link );	
							update_post_meta($attID,'is_cdn',1 );
						}
						update_post_meta($attID,'custom_price',$v_price );
						update_post_meta($attID, 'attribute_pa_color', $CLP_Helper->createSlug($v_color) );
						update_post_meta($attID, 'attribute_pa_size', $CLP_Helper->createSlug($v_size) );
						update_post_meta($attID, '_price', $v_price );
						update_post_meta($attID, '_regular_price', $v_price );
						update_post_meta($attID, '_sku', $post_name );
						update_post_meta($attID, '_virtual', 'no');
						update_post_meta($attID, '_downloadable', 'no');
						update_post_meta($attID, '_manage_stock', 'no');
						update_post_meta($attID, '_stock', 10000 );
						update_post_meta($attID, '_stock_status', 'instock');
						update_post_meta($attID, '_weight', @$v_weights[$key] );
					}
					if( isset($v_prices[0]) && $v_prices[0] != "" ){
						$v_prices[0] = str_replace(',','.',$v_prices[0]);
						update_post_meta( $pro_id,'custom_price',$v_prices[0] );
					}
					if( isset($v_colors[0]) && $v_colors[0] != "" ){
						update_post_meta($pro_id, 'attribute_pa_color', $CLP_Helper->createSlug($v_colors[0]) );
					}
					if( isset($v_sizes[0]) && $v_sizes[0] != "" ){
						update_post_meta($pro_id, 'attribute_pa_size', $CLP_Helper->createSlug($v_sizes[0]) );
					}
					//change feature image
				}
				/* Updata product meta */
				
				

				$product_sku 	= ( isset($data['sku']) && $data['sku'] != "" ) ? $data['sku'] : $CLP_Helper->createSlug($product_name);
				
				 update_post_meta( $pro_id, '_visibility', 'visible' );
				 update_post_meta( $pro_id, '_price', get_post_meta( $pro_id,'custom_price',true) );
				 update_post_meta( $pro_id, '_stock_status', 'instock');
				 update_post_meta( $pro_id, '_manage_stock', 'no');
				 update_post_meta( $pro_id, 'total_sales', '0');
				 update_post_meta( $pro_id, '_downloadable', 'no');
				 update_post_meta( $pro_id, '_virtual', 'no');
				 update_post_meta( $pro_id, '_purchase_note', "" );
				 update_post_meta( $pro_id, '_featured', "no" );
				 update_post_meta( $pro_id, '_length', "" );
				 update_post_meta( $pro_id, '_width', "" );
				 update_post_meta( $pro_id, '_height', "" );
				 update_post_meta( $pro_id, '_sku', $product_sku);
				 update_post_meta( $pro_id, '_sale_price_dates_from', "" );
				 update_post_meta( $pro_id, '_sale_price_dates_to', "" );
				 update_post_meta( $pro_id, '_sold_individually', "" );
				 update_post_meta( $pro_id, '_backorders', "no" );
				
				$update_post = array(
				  'ID'           	=> $pro_id,
				  'post_status'   	=> 'publish'
				);
				wp_update_post( $update_post );
				
				/* Fix missing color but no luck */
				$postdata = http_build_query(
					array(
						'action' 		=> 'woocommerce_save_attributes',
						'product_type'  => 'variable',
						'post_id'  		=> $pro_id,
						'data'  		=> $data_attributes,
						'security' 		=> wp_create_nonce( 'woocommerce_save_attributes' )
					)
				);
				$opts = array('http' =>
					array(
						'method'  => 'POST',
						'header'  => 'Content-type: application/x-www-form-urlencoded',
						'content' => $postdata
					)
				);
				// $context  = stream_context_create($opts);
				// #Note Error Error 6
				// $result = file_get_contents(home_url().'/wp-admin/admin-ajax.php', false, $context);
				// #Note Error Error 5
				// file_get_contents(home_url().'/wp-admin/post.php?post='.$pro_id.'&action=edit');
				/* End fix mising colors */
				
				 /* Updata product description */	

				if( $product_featured_img ){
					// If google will be rederect orther link:
					$product_featured_img 	= $this->_get_redirect_target($product_featured_img);
					$push_to_sheet_img = "";

					// Imgae to push google mer chant
					// $product_featured_img 	= $product_featured_img;
					if( !$this->_check_is_image($product_featured_img) ){
						wp_delete_post( $pro_id,true);
						$push_to_sheet = false;
					} 
					// Begin To URl Image
					// 1. Doi Link
					//  $product_featured_img 	= str_replace('=w1500-h1400','=w800-h400', $product_featured_img);

					$product_featured_img_name = $this->to_wp_img($product_handle."-".$pro_id);
					 
					//  End Url Image
					$sql = "SELECT post_id FROM $wpdb->postmeta WHERE  meta_key = 'featured_img_name' AND meta_value = '".sanitize_text_field($product_featured_img_name)."'";
					$featured_pro_id = $wpdb->get_var($sql);
					$featured_pro_id = 0;
					$download_method = 'download';	
					if (!$featured_pro_id || $featured_pro_id < 1) {
						if( $this->is_redirect($product_featured_img)){
							$product_featured_img = $this->_download_img_from_url($product_featured_img_name,$product_featured_img);
							// var_dump($product_featured_img);
							

							// var_dump($product_featured_img);
						
							// if( file_exists( $product_featured_img ) ){
								$attach_id 		= $this->_insert_attachment_post( $pro_id,$product_featured_img,$product_sku );													
								if( $attach_id ){
									$product_featured_link = wp_get_attachment_image_url($attach_id,'full');
									update_post_meta($pro_id,'_thumbnail_id',$attach_id );
									update_post_meta($pro_id,'featured_img_name',$product_featured_img_name );	
									update_post_meta($pro_id,'custom_img_url',$product_featured_link );
								}
							// }
						}else{
							$attach_id 		= $this->_insert_cdn_attachment_post( $pro_id,$product_featured_img_name,$product_featured_img,$product_sku );
							if( $attach_id ){
								$product_featured_link = wp_get_attachment_image_url($attach_id,'full');				
								update_post_meta($pro_id,'_thumbnail_id',$attach_id );
								update_post_meta($pro_id,'featured_img_name',$product_featured_img_name );
								update_post_meta($pro_id,'custom_img_url',str_replace("https://","//",$product_featured_img) );
								update_post_meta($attach_id,'is_cdn',1 );
							}
						}
						
					}else{
						$sql = "SELECT meta_value FROM $wpdb->postmeta WHERE post_id = $featured_pro_id AND meta_key = '_thumbnail_id'";
						$featured_img_id = $wpdb->get_var($sql);
						update_post_meta($pro_id,'_thumbnail_id',$featured_img_id );
						$product_featured_link = wp_get_attachment_image_url($featured_img_id,'full');	
						update_post_meta($pro_id,'custom_img_url',$product_featured_link );
					}					
				}else{
					$update_post = array(
					  'ID'           	=> $pro_id,
					  'post_status'   	=> 'trash'
					);
					wp_update_post( $update_post );
					$push_to_sheet = false;
				}
				
				
				

				if( $post_content != '' ){
					$pee = $post_content;
					$pee = preg_replace('|<br\s*/?>\s*<br\s*/?>|', "", $pee);
 
				    $allblocks = '(?:table|thead|tfoot|caption|col|colgroup|tbody|tr|td|th|div|dl|dd|dt|ul|ol|li|pre|form|map|area|blockquote|address|math|style|p|h[1-6]|hr|fieldset|legend|section|article|aside|hgroup|header|footer|nav|figure|figcaption|details|menu|summary)';
				 
				    // Add a double line break above block-level opening tags.
				    $pee = preg_replace('!(<' . $allblocks . '[\s/>])!', "$1", $pee);
				 
				    // Add a double line break below block-level closing tags.
				    $pee = preg_replace('!(</' . $allblocks . '>)!', "$1", $pee);
				    // Standardize newline characters to "\n".
   					$pee = str_replace(array("\r\n", "\r"), "", $pee);
					$update_post = array(
					  'ID'           	=> $pro_id,
					  'post_content'   	=> $pee,
					  'post_date'   	=> date( 'Y-m-d H:i:s',time() )
					);
					/*
					//wp_update_post( $update_post );
					*/
					$wpdb->query( 
						$wpdb->prepare( 
							"
							UPDATE $wpdb->posts 
							SET post_content = %s
							WHERE ID = %d
							",
							$pee,$pro_id
						) 
					);
				}

				if( $push_to_sheet  ){

					/* PUSH DATA TO GOOGLE SHEET */
					$img_url = get_post_meta( $pro_id,'custom_img_url',true);
					if(!preg_match("/https\:\/\//i", $img_url)){
						$img_url = "https:".$img_url;
					}
					$push_data = [
						"id"                          => (float)trim($pro_id),
						"title"                       => $title,
						"description"                 => $post_content,
						"item group id"               => "",            
						"link"                        => get_the_permalink($pro_id),    
						"product type"                => $data['product_cat'],            
						"age group"                   => "adult",        
						"google product category"     => 
								( $categories!="" ) ? $categories : 'Apparel & Accessories > Clothing > Shirts & Tops',                      
						"image link"                  => 
								change_size_img_cdn($img_url,"google_size"),          
						"condition"                   => "",        
						"availability"                => "In Stock",            
						"price"                       => 
								trim(get_post_meta( $pro_id,'custom_price',true).' USD'),    
						"brand"                       => $product_manufacture,    
						"gender"                      => "unisex",      
						"shipping weight"             => "188",              
						"color"                       => 
								get_post_meta($pro_id, 'attribute_pa_color',true),    
						"size"                        => get_post_meta($pro_id, 'attribute_pa_size',true),    
						"identifier exists"           => ""
					];
					
					$type = $data['product_cat'];
					
					if( 
						trim(strtolower($type)) == "coffee mug" || 
						trim(strtolower($type)) == "sticker landscape" || 
						trim(strtolower($type)) == "sticker portrait" ||
						trim(strtolower($type)) == "tote bag"
						) {
							$push_data['identifier exists'] = "FALSE";
						}
						if(!$push_data['image link'] && $push_data['image link'] == ""){
							// Do Nothing											
						}else{
							require_once CLP_ABSPATH.'/libraries/process.php';
							push_to_sheet($push_data);
						}
					}
				
				return $pro_id;
			}
		}
		


		// #UnknowContent
		// Co the la ham helper
		function _download_img_from_url($name,$img_url){
			preg_match("/\.png/i", $img_url, $ext);
			preg_match("/\.jpg/i", $img_url, $ext);
			if(!$ext){
				$ext = ".png";
			}
			
			if(preg_match("/drive.google.com/i", $img_url)
			  || preg_match("/vangogh\.teespring\.com\/mockup\.jpg\?composition/i", $img_url)
			){
				$image_name = $name.$ext[0]	;
				$url = $img_url;
				$img = CLP_ABSPATH.'tmp_images/'.$image_name;
				// #NoteErro Error 1:
				file_put_contents($img, file_get_contents($url));
				return $img;
			}
			if(preg_match("/https\:\/\//i", $img_url)){
				$img_url = str_replace("https://","//",$img_url);
			}
			return $img_url;
		}
		
		function _insert_attachment_galery_post($post_id,$images,$product_sku){
			$i = 1;
			$attach_ids = array();
			foreach ($images as $image) {
				if( file_exists($image) ){
					$attach_ids[] = $this->_insert_attachment_post($post_id,$image,$product_sku,$i );
					$i++;
				}
			}

			if( count($attach_ids) > 0){
				$attach_ids = implode(',',$attach_ids);
				update_post_meta($post_id,'_product_image_gallery',$attach_ids);

				$attach_ids = explode(',',$attach_ids);
			}
			return $attach_ids;
		}

		private function _insert_cdn_attachment_post($post_id,$product_featured_img_name,$product_featured_img,$product_sku = '',$i = 0 ){
			preg_match("/\.png/i", $product_featured_img, $match);

			if($match){
				$ext = "png";
			} else {
				$ext = "jpg";
			}

			$upload_path = wp_upload_dir();

			$type_image = '	image/jpeg';
			if( $ext == 'png' ){
				$type_image = 'image/png';
			}
			$tmp_file_info = array(
				'name' 		=> $product_featured_img_name.$ext,
				'type'		=> $type_image,
				'tmp_name' 	=> $product_featured_img,
			);
			$name = $tmp_file_info['name'];
			$attachment = array(
	          'post_mime_type' 	=> 'image/png',
	          'post_title' 		=> $product_sku,
	          'post_name' 		=> $product_sku,
	          'post_content' 	=> '',
	          'post_status' 	=> 'inherit'
	        );

	       global $wpdb;
	       $attach_id =$wpdb->insert( 
				$wpdb->posts, 
				array( 
					'post_title' 	 => $product_sku, 
					'post_name' 	 => $product_sku, 
					'post_mime_type' => 'image/png', 
					'post_type' 	 => 'attachment', 
					'post_status' 	 => 'inherit', 
					'post_parent'	 => $post_id 
				), 
				array( 
					'%s', 
					'%s', 
					'%s', 
					'%s', 
					'%s', 
					'%d' 
				) 
			);
	      
	       if( $attach_id ){
	       		$attach_id = $wpdb->insert_id;
	       		update_post_meta( $attach_id, '_wp_attached_file', $product_featured_img );
	       }

	       return $attach_id;

			

	        $attach_id = wp_insert_attachment($attachment, $product_featured_img, $post_id);

	        return $attach_id;

		}

		function tool_upload_dir($uploads ){
			$uploads['baseurl'] 	= '';
		    return $uploads;
		}
		private function _insert_attachment_post($post_id,$product_featured_img,$product_sku = '',$i = 0 ){


			$ext = explode('.',$product_featured_img);
			$ext = end($ext);

			$type_image = '	image/jpeg';
			if( $ext == 'png' ){
				$type_image = 'image/png';
			}

			$tmp_file_info = array(
				'name' 		=> $product_sku.'.'.$ext,
				'type'		=> $type_image,
				'tmp_name' 	=> $product_featured_img,
			);

			
			
			$upload_path = wp_upload_dir();
			$name = $tmp_file_info['name'];
			if ( @copy( $tmp_file_info['tmp_name'], $upload_path['path'] . '/' . $name ) ) {
				@unlink( $product_featured_img );
				// var_dump($product_featured_img);
			}

	       

	        $pathurl = $upload_path['url'];
	        $attachment = array(
	          'post_mime_type' 	=> 'image/png',
	          'post_title' 		=> $product_sku,
	          'post_name' 		=> $product_sku,
	          'post_content' 	=> '',
	          'post_status' 	=> 'inherit'
	        );

	       global $wpdb;
	       $attach_id =$wpdb->insert( 
				$wpdb->posts, 
				array( 
					'post_title' 	 => $product_sku, 
					'post_name' 	 => $product_sku, 
					'post_mime_type' => 'image/png', 
					'post_type' 	 => 'attachment', 
					'post_status' 	 => 'inherit', 
					'post_parent'	 => $post_id 
				), 
				array( 
					'%s', 
					'%s', 
					'%s', 
					'%s', 
					'%s', 
					'%d' 
				) 
			);
	      
	       if( $attach_id ){
	       		$attach_id = $wpdb->insert_id;
	       		update_post_meta( $attach_id, '_wp_attached_file', $upload_path['path'] . '/' . $name );
	       }

	       return $attach_id;

	       


	        $attPost = array(
	        	'file' 		  	=>  $upload_path['path'] . '/' . $name,
	        	'post_parent' 	=> $post_id,
	        	'post_type' 	=> 'attachment',
	        	'post_title' 	=> $attachment['post_title'],
	        	'post_name' 	=> $attachment['post_name'],
	        	'post_name' 	=> $attachment['post_mime_type'],
	        	'post_status' 	=> $attachment['post_status'],
	        );

	        $attach_id = wp_insert_post( $attPost);  
	        $attach_id  = @wp_insert_attachment($attachment, $upload_path['path'] . '/' . $name, $post_id,true);	    
 
	        $attach_data = wp_generate_attachment_metadata($attach_id, $upload_path['path'] . '/' . $name);
	        wp_update_attachment_metadata($attach_id, $attach_data);
	        return $attach_id;
		}

		private function _check_exist_pro( $post_title ){
			global $wpdb;
			$sql = "SELECT ID FROM wp_posts WHERE post_name =  '".$post_title."' AND post_type = 'product'";	
			$check_exist_pro_id   = $wpdb->get_var( $sql );
			return $check_exist_pro_id;
		}

		private function _insert_post($pro_title , $pro_name ){
			$data['post_title'] 	= sanitize_text_field( $pro_title );
			$post_name = ( $pro_name != "" ) ? $pro_name : $pro_title;
			$data['post_name'] 		= $post_name;
		    $data['post_status'] 	= 'publish';
		    $data['post_author'] 	= 1;
		    $data['post_type'] 		= 'product';
			
			global $wpdb;
			
			$post_id = $wpdb->insert( 
					$wpdb->posts, 
					array( 
						'post_title' => sanitize_text_field( $pro_title ),
						'post_name' => $post_name,
						'post_status' => 'publish',
						'post_type' => 'product',
						'post_author' => 1,
					), 
					array( 
						'%s', 
						'%s', 
						'%s', 
						'%s', 
						'%d' 
					) 
				);
				if( $post_id ){
					$post_id = $wpdb->insert_id;
			}
		    return $post_id;
		}
		private function _get_cat_id( $category_name,$parent_term_id = 0 ){
			$taxonomy = 'product_cat';
			$term = get_term_by('name', $category_name, $taxonomy );
			
			if( !$term ){
				$args = array('parent' => $parent_term_id);
				$term_name = $category_name;
				$term_id = wp_insert_term( 
					$term_name, 
					$taxonomy ,
					$args
					);
				return $term_id['term_id'];
			}else{
				return $term->term_id;
			}
			
		}
		private function _get_attribute_id( $attribute_name,$taxonomy = 'pa_manufacturers',$parent_term_id = 0 ){
			if( $attribute_name != "" ){
				$term = get_term_by('name', $attribute_name, $taxonomy );

				if( !$term ){
					$args = array('parent' => $parent_term_id);
					$term_name = $attribute_name;

					$term_id = wp_insert_term( 
						$term_name, 
						$taxonomy
					);



					if( count( $term_id->errors ) > 0 ){
						return false;
					}else{
						return $term_id['term_id'];
					}
				}else{
					return $term->term_id;
				}
			}
		}
		private function _get_manufactory_id( $factory_name,$parent_term_id = 0 ){
			$taxonomy = 'pa_manufacturers';
			$term = get_term_by('name', $factory_name, $taxonomy );

			
			if( !$term ){
				$args = array('parent' => $parent_term_id);
				$term_name = $factory_name;
				$term_id = wp_insert_term( 
					$term_name, 
					$taxonomy ,
					$args
					);
				if( count( $term_id->errors ) > 0 ){
					return false;
				}else{
					return $term_id['term_id'];
				}
			}else{
				return $term->term_id;
			}
		}
		private function is_redirect($url){
			if(preg_match("/drive.google.com/i", $url)
			  || preg_match("/vangogh\.teespring\.com\/mockup\.jpg\?composition/i", $url)
			){
				return true;
			}
			return false;
		}
		private function _get_redirect_target($url){
			//return $url;
			if( !function_exists('curl_version') ) return $url;
			
			// If is google Link :
			if(preg_match("/drive.google.com/", $url)){
				$ch = curl_init($url);
			    curl_setopt($ch, CURLOPT_HEADER, 1);
			    curl_setopt($ch, CURLOPT_NOBODY, 1);
			    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			    $headers = curl_exec($ch);
			    curl_close($ch);
			    if (preg_match('/^Location: (.+)$/im', $headers, $matches))
			        return trim($matches[1]);
			}
		    return $url;
		}

		private function to_wp_img($name){
			$product_featured_img_name = preg_replace("/[^A-Za-z0-9]/", '-', $name);
			$product_featured_img_name = strtolower($product_featured_img_name);
			return $product_featured_img_name;
		}
		
		private function _check_is_image($url)
		{
			return true ;
			$url = change_size_img_cdn($url,"10x10");
			$image_header = get_headers($url,1);
			preg_match("/image/", $input_line, $content_type_match);

			if($content_type_match[0] == "image"){
				return true;
			}
			return false;
		}
}
