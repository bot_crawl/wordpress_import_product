const TaskImport = function (site_url = "" , progress_ele = "") {
// Start Import
  this.run = loop_import()
    .then((dataMsg) => {
      console.log(dataMsg);
    alert('Done ! Import New File');
    }).catch((dataMsg) => {
      location.reload();
      // console.log(dataMsg);
    // alert("Error Refresh Page"); 
  });
  
  // Private Method
  async function import_ajax() {
    let result = await jQuery.get(site_url,
      {"docron":1},
      (res) => {
        return res.responseText;
      }, "JSON");
    
    return result;
  };
  async function loop_import() {
    let is_done = false, is_fail = false;
    var msg_return = {
      "status": "",
      "debug": "",
      "result":""
    };
    while (!is_done) {
      let t = await import_ajax();
      if (typeof t !== "object") { // If not Json ( 500 Error )
        is_done = true;
        is_fail = true;
        
        console.log(typeof t);
        msg_return.status = "Error Server";
        msg_return.debug += t;
      } else if (t === "Waiting task") { // If Done
        id_done = true;
        msg_return.status = "Done";
        msg_return.result = t;
      } else { // If processing
        console.log(t, progress_ele);
        msg_return.status = "success";
        msg_return.result = { "sheet": t.sheet, "percent": t.percent };
        update_progress(t.sheet, t.percent);
      }
    }
    return msg_return;
  }

  var get_progess_ele = function (sheet) {
    return jQuery('.progress[data-sheet="' + sheet + '"]').find('.progress-bar');
  }
  function update_progress(sheet , percent) {
    var active_progress = get_progess_ele(sheet);
    active_progress.attr('aria-valuenow',percent);
    active_progress.attr('style','width: '+percent+'%');
    active_progress.addClass('progress-bar-info');
    active_progress.text(percent+'% complete');
  }
  // End Private Method

}