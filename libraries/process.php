<?php		

require_once 'autoload.php';

require_once 'base.php';
define('SCOPES', implode(' ', array(
	Google_Service_Sheets::SPREADSHEETS)
  ));

  
$srcDir = realpath(__DIR__ . '/src/');
restore_include_path ();
set_include_path($srcDir . PATH_SEPARATOR . get_include_path());

// spl_autoload_register(function ($class) {
// 	if (strpos($class, '\\') !== false) {
// 		@include_once str_replace("\\", "/", $class) . '.php';
// 	}
// });

function invalidServiceAccount() {
	$client = new Google_Client();
	if ($credentials_file = checkServiceAccountCredentialsFile()) {
		// set the location manually
		$client->setAuthConfig($credentials_file);
	} elseif (putenv('GOOGLE_APPLICATION_CREDENTIALS=service-account-credentials.json')) {
		// use the application default credentials
		$client->useApplicationDefaultCredentials();
	} else {
		echo missingServiceAccountDetailsWarning();
		return false;
	}
	// If invalid , set info
	$client->setApplicationName("Google Sheets API PHP Quickstart'");
	$client->setScopes(SCOPES);

	return $client;
}

function push_to_sheet($data) {

	$client = invalidServiceAccount();
	if(!$client){
		return false;
	}

	$service = new Google_Service_Sheets($client);

	/************************************************
	  We're just going to make the same call as in the
	  simple query as an example.
	 ************************************************/
	$requestBody = new Google_Service_Sheets_ValueRange();
	$requestBody->setValues(
		[
		  array_values($data)
		]
	);
	$optionsRequestBody = [
		"includeValuesInResponse"=>FALSE,
		"insertDataOption"=>"INSERT_ROWS",
		"responseDateTimeRenderOption"=>"FORMATTED_STRING",
		"responseValueRenderOption"=>"FORMATTED_VALUE",
		"valueInputOption"=>"RAW"
	];
	$spreadsheetId = '1rPPBhm-XFfk8hCXNfj1kcydWdE1oR5OYq20xYHp-Xpo';
	$range = '1. Template!A:R';
	try{
		$response = $service->spreadsheets_values
		->append(
		  $spreadsheetId, 
		  $range,
		  $requestBody,
		  $optionsRequestBody
		);
	} catch (Google_Service_Exception $GSE){
		return false;
	}

	if(gettype($response) == "object"){
		return true;
	}
}