<style type="text/css">
    .form-control {
        margin-left: 0px !important;
    }
</style>
<div style="clear:both;margin-top:10px;"></div>
<?php
    if( count($alerts) > 0 ){
        ?>
            <div class="container">       
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Please fix folowing errors</h3>
                            </div>
                            <div class="panel-body">
                                <ul>
                                    <?php
                                        foreach ($alerts as $alert) {
                                            echo '<li style="color:red;">'.$alert.'</li>';
                                        }
                                    ?>
                                </ul>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php
    }
?>
<div class="container">       
    <div class="row">
        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-clock-o fa-fw"></i> Import Products</h3>
                </div>
                <div class="panel-body">
                  
                    <?php 
                        if( count($alerts) > 0 ){
                            require 'dashboard-form.php';
                        }
                    ?>
                  
                </div>
            </div>
        </div>
        <div id="right">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-clock-o fa-fw"></i> Processing</h3>
            </div>
            <div class="panel-body txt_process">
               
            </div>
        </div>
        </div>
    </div>

</div>