<?php
/*
Plugin Name: Import Products
Plugin URI: thelifecode.net
Description: Import Products
Version: 0.0.1
Author: taitamfc
Author URI: thelifecode.net
License: GPL2
*/
//error_reporting(-1);
if(  
	is_admin() ||
	(
		isset($_GET['page']) && $_GET['page'] == 'import-products'
	) || 
	(
		isset($_GET['docron']) || isset($_GET['stopcron'])
	)
 ) {
	//  Do Nothing
 } else {
	 return ;
 }

define( 'CLP_ABSPATH' , trailingslashit( WP_PLUGIN_DIR . '/' . str_replace(basename( __FILE__ ) , "" , plugin_basename( __FILE__ ) ) ) );
define( 'CLP_URI'     , trailingslashit( WP_PLUGIN_URL . '/' . str_replace(basename( __FILE__ ) , "" , plugin_basename( __FILE__ ) ) ) );

$upload_dir = wp_upload_dir();
define('CLP_UPLOADIR',$upload_dir['basedir'].'/clp/');

require_once CLP_ABSPATH .'/classes/clp-helper.php';
require_once CLP_ABSPATH .'/classes/clp-data.php';
require_once CLP_ABSPATH .'/classes/clp-model.class.php';
require_once CLP_ABSPATH .'/classes/clp-admin.class.php';
require_once CLP_ABSPATH .'/classes/clp-ajax.class.php';

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

$CPL_Admin = new CPL_Admin();


add_action('init','tool_init');
function tool_init (){
	// add_filter( 'wp_get_attachment_url', 'tool_wp_get_attachment_url', 99, 2 );
	 // #Note Error 4
	// add_filter( 'wp_get_attachment_image_src', 'tool_wp_get_attachment_image_src', 99, 2 );
	// #Note Error 3
	// add_action('save_post','tool_save_post');
	// echo '<pre>';
	// var_dump( wp_get_attachment_image_src( 239699 ) );
	// die();
	
	if( isset($_GET['stopcron']) ){
		$options['task'] = 'waiting';
		update_option( 'clp_options' ,$options);
		echo 'Cron stopped !';
		die();
	}
	if( isset($_GET['docron']) ){
		$cron_options = get_option( 'clp_options');
		// echo '<pre>';
		// print_r($cron_options);
		// echo '</pre>';
		$CLP_Data = new CLP_Data();
		$CLP_Data->_params = $cron_options;
		$return = $CLP_Data->start();
		die();
	}

 	/*
 	global $wpdb;
	// DELETE FROM `wp_posts` WHERE `post_type` = 'product';
	// DELETE FROM `wp_posts` WHERE `post_type` = 'product_variation';
	// DELETE FROM `wp_postmeta` WHERE `meta_key` = 'featured_img_name';
	// DELETE FROM `wp_postmeta` WHERE `meta_key` = 'v_featured_img_name';

	# First:
 
	DELETE FROM wp_postmeta
	WHERE post_id IN
	(
	SELECT id
	FROM wp_posts
	WHERE post_type = 'attachment'
	)
	;
	 
	# Second:
	 
	DELETE FROM wp_posts WHERE post_type = 'attachment'
	
 	$sql  = "SELECT ID FROM $wpdb->posts as p
						INNER JOIN $wpdb->term_relationships as tr ON ID = tr.object_id
						INNER JOIN $wpdb->term_taxonomy as tt ON tt.term_taxonomy_id = tr.term_taxonomy_id
						INNER JOIN $wpdb->terms as t ON t.term_id = tt.term_id
						WHERE post_type = 'product' AND post_status = 'publish' ";
	$sql .= ' GROUP BY p.ID';
				
	$results = $wpdb->get_results($sql);
	if( count($results) > 0 ){
		foreach ($results as $result) {
			$pro_id = $result->ID;
			update_post_meta( $pro_id, '_price', get_post_meta( $pro_id,'custom_price',true) );
		}
	}
	die();
	*/
 	
}


function get_redirect_target($url){

    if( !function_exists('curl_version') ) return $url;
   
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_NOBODY, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $headers = curl_exec($ch);
    curl_close($ch);
    // Check if there's a Location: header (redirect)
    if (preg_match('/^Location: (.+)$/im', $headers, $matches))
        return trim($matches[1]);
    // If not, there was no redirect so return the original URL
    // (Alternatively change this to return false)
    return $url;
}
function get_element_string_by_xml($string)
{
    $dom = new DOMDocument();
    @$dom->loadHTML($string);
    $htmlString = $dom->saveHTML();
    return $htmlString;
}

function get_redirect_final_target($url){

     if( !function_exists('curl_version') ) return $url;
   
   
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_NOBODY, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // follow redirects
    curl_setopt($ch, CURLOPT_AUTOREFERER, 1); // set referer on redirect
    curl_exec($ch);
    $target = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
    curl_close($ch);
    if ($target)
        return $target;
    return $url;
}